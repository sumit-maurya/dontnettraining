﻿// See https://aka.ms/new-console-template for more information
using FoodApp_Hackathon_Day8.Reposetory;

Console.WriteLine("Enter the Username::");
string userName = Console.ReadLine();

Console.WriteLine($"\n--------Wellcome {userName}-------\n ");

FoodReposetory objFoodReposetory = new FoodReposetory();

PincodeEntry:
Console.WriteLine("Enter Your Pincode::");
int pinCode = int.Parse(Console.ReadLine());

if (!objFoodReposetory.VarifyPinCode(pinCode))
{
    Console.WriteLine("\n !!!  At this Location Our Service is not available , Please Try again !!! \n \n// 400068 , 400103, 400104 , 400561 , 400833  //\n");
    goto PincodeEntry;
}

//View Menu,OrderPizza,View Cart, Update the Cart,Pay bill From the Cart,Exit

Console.WriteLine("\n+---------------------------------------------------------------------------------------------------+");
Console.WriteLine("| Add Food to Cart:1  |  View Cart:2  |  Remove Cart:3  |  Pay Bill:4  |  Update Cart:5  |  Exit:6  |");
Console.WriteLine("+---------------------------------------------------------------------------------------------------+\n");

Console.WriteLine("Enter Your Choice::");
string? choice = Console.ReadLine();

while (choice != "6")
{
    if (choice == "1")
    {
        objFoodReposetory.ShowMenu();

        Console.WriteLine("\nenter the FoodId for add food to Cart");
        string idChoice = Console.ReadLine();

        Console.WriteLine("Enter Quantity::");
        int quantity=int.Parse(Console.ReadLine());

        objFoodReposetory.AddFoodToCart(idChoice,userName,quantity);
        objFoodReposetory.ViewCart(userName);
    }
    else if (choice == "2")
    {
        objFoodReposetory.ViewCart(userName);
    }
    else if (choice == "3")
    {
       objFoodReposetory.RemoveFoodFromCart(userName);
       objFoodReposetory.ViewCart(userName);
    }
    else if (choice == "4")
    {
        objFoodReposetory.PayBill(userName, pinCode);
    }
    else if (choice == "5")
    {
        objFoodReposetory.UpdateFoodToCart(userName);
        objFoodReposetory.ViewCart(userName);
    }

    Console.WriteLine("\n+---------------------------------------------------------------------------------------------------+");
    Console.WriteLine(  "| Add Food to Cart:1  |  View Cart:2  |  Remove Cart:3  |  Pay Bill:4  |  Update Cart:5  |  Exit:6  |");
    Console.WriteLine(  "+---------------------------------------------------------------------------------------------------+\n");

    Console.WriteLine("Enter Your Choice::");
    choice = Console.ReadLine();
}


