﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Exceptions
{
    internal class FoodNotAvailableException:Exception
    {
        public FoodNotAvailableException()
        {

        }
        public FoodNotAvailableException(string message):base(message)
        {

        }
    }
}
