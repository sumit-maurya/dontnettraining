﻿using FoodApp_Hackathon_Day8.Exceptions;
using FoodApp_Hackathon_Day8.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Reposetory
{
    internal class FoodReposetory : IFoodInterface
    {

        SqlConnection sqlcon = null;
        SqlCommand cmd = null;

        //    List<Food> foodItems;
        //    List<AvailableLocations> availableLocations;
        //    List<Food> foodCart = new List<Food>();

        public FoodReposetory()
        {
            sqlcon = new SqlConnection("Server=DESKTOP-VUA8SUS\\MSSQLSERVER01;Database=FoodDb;Trusted_Connection=True");

            #region Old code : implementation with List 
            //    foodItems = new List<Food>()
            //        {
            //            new Food(){ FoodId=101,FoodName="Veg Shawrma     ",FoodType="Veg",Price=100 },
            //            new Food(){ FoodId=102,FoodName="Fried Rice      ",FoodType="Veg",Price=200 },
            //            new Food(){ FoodId=103,FoodName="Dal             ",FoodType="Veg",Price=150 },
            //            new Food(){ FoodId=104,FoodName="Biryani         ",FoodType="Veg",Price=300 },
            //            new Food(){ FoodId=105,FoodName="chaat           ",FoodType="Veg",Price=150 },
            //            new Food(){ FoodId=106,FoodName="Tandoori Chicken",FoodType="NonVeg",Price=100 },
            //            new Food(){ FoodId=107,FoodName="BBQ Chicken     ",FoodType="NonVeg",Price=460 },
            //            new Food(){ FoodId=108,FoodName="Chicken Shawrma ",FoodType="Nonveg",Price=75 },
            //            new Food(){ FoodId=109,FoodName="Omlet           ",FoodType="NonVeg",Price=87 },
            //            new Food(){ FoodId=100,FoodName="Gulab Jamun     ",FoodType="Veg",Price=80 }
            //        };
            //    availableLocations = new List<AvailableLocations>()
            //    {
            //        new AvailableLocations(){PinCode=400068},
            //        new AvailableLocations(){PinCode=400103},
            //        new AvailableLocations(){PinCode=400104},
            //        new AvailableLocations(){PinCode=400561},
            //        new AvailableLocations(){PinCode=400833}
            //    };
            #endregion
        }
        public void AddFoodToCart(string foodId, string userName, int quantity)
        {
            int parsefoodId = int.Parse(foodId);
            sqlcon.Open();
            string query1 = "Select * from FoodItems  Where FoodId = @parsefoodId";
            cmd = new SqlCommand(query1, sqlcon);
            cmd.Parameters.AddWithValue("@parsefoodId", parsefoodId);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                Food foodItem = new Food();
                foodItem.FoodId = (int)reader["FoodId"];
                foodItem.FoodName = (string)reader["FoodName"];
                foodItem.FoodType = (string)reader["FoodType"];
                foodItem.Price = (int)reader["Price"];
                string query = "Select * from FoodCart where FoodId = @parsefoodId and UserName=@userName";
                SqlCommand cmd_checkCart = new SqlCommand(query, sqlcon);
                cmd_checkCart.Parameters.AddWithValue("@parsefoodId", parsefoodId);
                cmd_checkCart.Parameters.AddWithValue("@userName", userName);
                sqlcon.Close();
                sqlcon.Open();
                SqlDataReader reader2 = cmd_checkCart.ExecuteReader();
                if (reader2.Read())
                {
                    query = "Update FoodCart set Quantity=@quantity where FoodId=@foodId and UserName=@userName";
                    SqlCommand cmd2 = new SqlCommand(query, sqlcon);
                    cmd2.Parameters.AddWithValue("@foodId", (int)reader2["FoodId"]);
                    cmd2.Parameters.AddWithValue("@quantity", quantity + (int)reader2["Quantity"]);
                    cmd2.Parameters.AddWithValue("@userName", (string)reader2["UserName"]);
                    sqlcon.Close();
                    sqlcon.Open();
                    cmd2.ExecuteNonQuery();
                    Console.WriteLine("Food is added To Cart");
                    sqlcon.Close();
                    return;

                }
                else
                {
                    query = "insert into FoodCart values (@FoodId,@FoodName,@FoodType,@Price,@userName,@quantity)";
                    SqlCommand cmd2 = new SqlCommand(query, sqlcon);
                    cmd2.Parameters.AddWithValue("@FoodId", foodItem.FoodId);
                    cmd2.Parameters.AddWithValue("@FoodName", foodItem.FoodName);
                    cmd2.Parameters.AddWithValue("@FoodType", foodItem.FoodType);
                    cmd2.Parameters.AddWithValue("@Price", foodItem.Price);
                    cmd2.Parameters.AddWithValue("@userName", userName);
                    cmd2.Parameters.AddWithValue("@quantity", quantity);
                    sqlcon.Close();
                    sqlcon.Open();
                    cmd2.ExecuteNonQuery();
                    Console.WriteLine("Food is added To Cart");
                    sqlcon.Close();
                    return;
                }
            }
            sqlcon.Close();

            //foreach (Food food in foodItems)
            //{
            //    if (food.FoodId == int.Parse(foodId))
            //    {
            //        foodCart.Add(food);
            //        Console.WriteLine("Food is added To Cart");
            //        return;
            //    }
            //}
            Console.WriteLine("Food Item is Not Present in Menu !!!");
        }

        public void PayBill(string userName, int pinCode)
        {
            string query = "Select Count(*) from FoodCart where UserName=@userName ";
            cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.AddWithValue("@userName", userName);

            sqlcon.Open();
            int foodcount = Convert.ToInt16(cmd.ExecuteScalar());
            sqlcon.Close();

            if (foodcount == 0)
            {
                Console.WriteLine("Cart is Empty , Please  Order Food !!!");
                return;
            }

            query = "Select sum(Price*Quantity) from FoodCart where UserName=@userName ";
            cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.AddWithValue("@userName", userName);
            sqlcon.Open();
            int totalBillAmount = Convert.ToInt16(cmd.ExecuteScalar());
            sqlcon.Close();
            //foreach (Food food in foodCart)
            //{
            //    totalBillAmount += food.Price;
            //}
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            string second = DateTime.Now.Second.ToString();
            string invoiceNo = "#" + year + month + day + "-" + hour + minute + second;
            PrintBill(totalBillAmount, userName, pinCode, invoiceNo);

            query = "Delete from FoodCart where UserName=@userName";
            cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.AddWithValue("@userName", userName);

            sqlcon.Open();
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            //foodCart.Clear();
        }

        private void PrintBill(int totalBillAmount, string userName, int pinCode, string invoiceNo)
        {
            Console.WriteLine("  +--------------------------------------------+");
            Console.WriteLine("  |                  Invoice                   | ");
            Console.WriteLine("  +--------------------------------------------+");
            Console.WriteLine($"    Name: {userName}                           ");
            Console.WriteLine($"    Pincode: {pinCode}                         ");
            Console.WriteLine("                                             ");
            Console.WriteLine($"    Invoice No.::{invoiceNo}\n");
            string query = "Select * from FoodCart where UserName =@userName";
            cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.AddWithValue("@userName", userName);
            sqlcon.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"   {reader["FoodName"]}\t\t[{reader["Price"]}]x{reader["Quantity"]}...{(int)reader["Quantity"] * (int)reader["Price"]} Rs.  ");
            }
            sqlcon.Close();
            //foreach (Food food in foodCart)
            //{
            //    Console.WriteLine($" {food.FoodName}\t\t........{food.Price} Rs.  ");
            //}
            Console.WriteLine($"\n   Total Bill Amount::{totalBillAmount}Rs.     \n");
            Console.WriteLine($"   Date::{DateTime.Now.ToString()}\n");
            Console.WriteLine("  +--------------------------------------------+");
        }

        public void RemoveFoodFromCart(string userName)
        {
            try
            {
                if (FoodCartCount(userName) == 0)
                {
                    throw new CartIsEmptyException("\n!!! Cart is Empty Food can not be Remove !!!\n");
                }
                ViewCart(userName);
                Console.WriteLine("\nenter the FoodId for Remove the food item form the Cart:: ");
                string? foodId = Console.ReadLine();

                string query = "select * from FoodCart where FoodId=@foodId and UserName=@userName";
                cmd = new SqlCommand(query, sqlcon);
                cmd.Parameters.AddWithValue("@userName", userName);
                cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));

                sqlcon.Open();
                SqlDataReader existFood = cmd.ExecuteReader();

                if (existFood.Read())
                {
                    sqlcon.Close();

                    query = "Delete from FoodCart where FoodId=@foodId  and UserName=@userName";
                    cmd = new SqlCommand(query, sqlcon);
                    cmd.Parameters.AddWithValue("@userName", userName);
                    cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));

                    sqlcon.Open();
                    cmd.ExecuteNonQuery();
                    sqlcon.Close();

                    Console.WriteLine("FoodItem is Deleted successfully!!");
                    return;
                }

                #region Old CODE :List implementation ( removing the food item from FoodCart)
                //foreach (Food food in foodCart)
                //{
                //    if (food.FoodId == int.Parse(foodId))
                //    {
                //        foodCart.Remove(food);
                //        ViewCart();
                //        return;
                //    }
                //}
                #endregion
                sqlcon.Close();
                throw new FoodNotAvailableException("\nFood is Not Present in Cart !!!\n");
            }
            catch (FoodNotAvailableException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (CartIsEmptyException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool VarifyPinCode(int pinCode)
        {
            sqlcon.Open();
            string query1 = "Select * from AvailableLocation  Where PinCode = @PinCode";
            cmd = new SqlCommand(query1, sqlcon);
            cmd.Parameters.AddWithValue("@PinCode", pinCode);
            SqlDataReader reader = cmd.ExecuteReader();

            //var varifyPinCode = availableLocations.Where(x => x.PinCode == pinCode).FirstOrDefault();
            if (reader.Read())
            {
                sqlcon.Close();
                return true;
            }
            sqlcon.Close();
            return false;
        }

        public void UpdateFoodToCart(string userName)
        {
            try
            {
                if (FoodCartCount(userName) == 0)
                {
                    throw new CartIsEmptyException("\nCart is Empty, Food can not be Update !!!\n");   
                }
                ViewCart(userName);
                Console.WriteLine("enter the FoodId for Update the food item form the Cart:: ");
                string? foodId = Console.ReadLine();

                string query = "select * from FoodCart where FoodId=@foodId and UserName=@userName";
                cmd = new SqlCommand(query, sqlcon);
                cmd.Parameters.AddWithValue("@userName", userName);
                cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));

                sqlcon.Open();
                SqlDataReader existFood = cmd.ExecuteReader();
                if (existFood.Read())
                {
                    Console.WriteLine("Enter the New Quantity::");
                    int quantity = int.Parse(Console.ReadLine());
                    sqlcon.Close();

                    query = "Update FoodCart set Quantity=@quantity where FoodId=@foodId  and UserName=@userName";
                    cmd = new SqlCommand(query, sqlcon);
                    cmd.Parameters.AddWithValue("@userName", userName);
                    cmd.Parameters.AddWithValue("@quantity", quantity);
                    cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));

                    sqlcon.Open();
                    cmd.ExecuteNonQuery();
                    sqlcon.Close();

                    Console.WriteLine("FoodItem is Updated successfully!!");
                    return;
                }

                #region Old CODE : implmentation with list
                //foreach (Food food in foodCart)
                //{
                //    if (food.FoodId == int.Parse(foodId))
                //    {
                //        foodCart.Remove(food);
                //        ViewCart();
                //        return;
                //    }
                //}
                #endregion
                sqlcon.Close();

                throw new FoodNotAvailableException("\n!!! This Food is Not Present in Cart !!!\n");
            }
            catch (FoodNotAvailableException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (CartIsEmptyException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ViewCart(string userName)
        {
            Console.WriteLine("\t------------Your Cart-------------");

            if (FoodCartCount(userName) == 0)
            {
                Console.WriteLine("\t\t!!!  Cart is Empty !!!");
            }
            else
            {
                string query = "Select * from FoodCart where UserName=@userName";
                cmd = new SqlCommand(query, sqlcon);
                cmd.Parameters.AddWithValue("@userName", userName);

                sqlcon.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Console.WriteLine("\n\tFoodId\tFoodName     \tFoodType\tPrice\tQuantity");
                Console.WriteLine("\t-------------------------------------------------------\n");
                while (reader.Read())
                {
                    Console.WriteLine($"\t{reader["FoodId"]}\t{reader["FoodName"]}\t{reader["FoodType"]}\t{reader["Price"]}\t{reader["Quantity"]}");
                }
                sqlcon.Close();

                //foreach (Food food in foodCart)
                //{
                //    Console.WriteLine(food);
                //}
            }
        }

        public void ShowMenu()
        {
            string query = "Select * from FoodItems";
            cmd = new SqlCommand(query, sqlcon);

            sqlcon.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            Console.WriteLine("\n\tFoodId\tFoodName\t\tFoodType\tPrice");
            Console.WriteLine("\t-------------------------------------------------------\n");
            while (reader.Read())
            {
                Console.WriteLine($"\t{reader["FoodId"]}\t{reader["FoodName"]}\t{reader["FoodType"]}\t\t{reader["Price"]}");
            }
            sqlcon.Close();
        }

        public int FoodCartCount(string userName)
        {
            string query = "Select Count(*) from FoodCart where UserName=@userName";
            cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.AddWithValue("@userName", userName);

            sqlcon.Open();
            int foodcount = Convert.ToInt16(cmd.ExecuteScalar());
            sqlcon.Close();

            return foodcount;
        }
    }
}
