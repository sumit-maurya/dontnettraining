﻿using FoodApp_Hackathon_Day8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Reposetory
{
    internal interface IFoodInterface
    {
        //(ex:Addpizza,AddToCart,DeletePizza,ViewCart, IndividualBill,CartTotal,UpdatePizza,GetPizza)
        public void AddFoodToCart(string foodId,string userName,int quantity);

        public void RemoveFoodFromCart(string userName);
        public void UpdateFoodToCart(string userName);
        public void ViewCart(string userName);
        public void PayBill(string userName,int pinCodes);

    }
}
