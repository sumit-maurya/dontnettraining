﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Model
{
    internal class Food
    {
        public int FoodId { get; set; }
        public string? FoodName { get; set; }
        public string? FoodType { get; set; }
        public int Price { get; set; }

        public override string ToString()
        {
            return $"{FoodId}\t{FoodName}\t{FoodType}\t{Price}";
        }
    }
}
