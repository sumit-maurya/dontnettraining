﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace RegularExpresion
{
    internal static class validate
    {
        public const string pattern = @"^[0-9a-zA-Z!@#$%^&*()]{8,}$";
        public static bool validation(string pass)
        {
            if (pass != null)
            {
                return Regex.IsMatch(pass, pattern);
            }
            return false;
        }
    }
}
