﻿// See https://aka.ms/new-console-template for more information

namespace DelegateDemo
{
     class DelegateClass
    {
        public delegate int DelegateTosquare(int num1,int num2);
        public delegate void DelegateTosquare1(int num1, int num2);
        public delegate bool DelegateToCount(string message );
        static void Main()
        {

        } 
        public static int Square(int num1,int num2)
        {
            return num1 * num2;
        }
        public static void Square1(int num1 ,int num2)
        {
            Console.WriteLine(num1*num2);
        }

        public static bool CountLetter(string message)
        {
            if (message.Count > 4)
            {
                return true;
            }
            return false;
        }


    }
}