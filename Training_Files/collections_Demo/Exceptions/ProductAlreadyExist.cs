﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace collections_Demo.Exceptions
{
    internal class ProductAlreadyExist: Exception
    {
        public ProductAlreadyExist()
        {
                
        }
        public ProductAlreadyExist(string message):base(string message)
        {

        }
    }
}
