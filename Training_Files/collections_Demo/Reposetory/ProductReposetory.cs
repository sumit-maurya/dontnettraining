﻿using collections_Demo.Model;
using 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections_Demo.Reposetory
{
    internal class ProductReposetory
    {
        List<Product> products;

        public ProductReposetory()
        {
            products = new List<Product>()
            {
                new Product() {Id=1, Price=28336,Name="TV" },
                new Product() {Id=2, Price=747490,Name="Phone"}
            };

        }

        public List<Product> GetAllProduct()
        {
            return products;
        }

        public string AddProduct(Product product)
        {
            var returnProduct = GetProductByName(product.Name);
            if (returnProduct == null)
            {
                products.Add(product);
                return "Product is Added Successfully";
            }
            else
            {
                throw ProductAlreadyExist($"{product.Name} is Already Present");
            }
        }

        public object GetProductByName(string name)
        {
            return products.Find(p => p.Name == name);
        }
    }
}
