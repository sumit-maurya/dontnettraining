﻿// See https://aka.ms/new-console-template for more information
using LinQDemo;
Product[] productArray = new Product[]
{
    new Product{Name="g1",Category="C1",Make="M1",Price=2001},
     new Product{Name="G2",Category="C2",Make="M2",Price=15410},
     new Product{Name="G3",Category="C3",Make="M3",Price=1083},
     new Product{Name="G4",Category="C4",Make="M4",Price=2000}
};
Console.WriteLine("----LINQ----");
Product[] filterArray= productArray.Where(x => x.Price>2000&&x.Price<20000).ToArray();
foreach(Product item in filterArray)
{
    Console.WriteLine(item);
}