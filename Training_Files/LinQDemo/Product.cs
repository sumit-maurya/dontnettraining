﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQDemo
{
    internal class Product
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string Make { get; set; }
        public int Price { get; set; }
        public override string ToString()
        {
            return $"Name::{Name}\tCatagory::{Category}\tMake::{Make}\tPrice::{Price}";
        }
    }
}
