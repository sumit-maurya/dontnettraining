using Tempcon;
namespace Nunit_TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            //Initialization
            var temp1 = 32;
            var expected = 89.6;
            //Act
            //Call the Development Code
            tempclass obj = new tempclass();
            double actual = obj.Cel2Fer(temp1);
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void Test2()
        {
            //Initialization
            var temp1 = 36;
            var expected = -12;
            //Act
            //Call the Development Code
            tempclass obj = new tempclass();
            double actual = obj.Fer2Cel(temp1);
            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}