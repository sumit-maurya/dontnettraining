﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Reposetory
{
    internal interface IFile
    {
        public void WriteToFile(User user, string filname);

        //public bool IsNameThere(string name, string filname);

        public void ReadFromFile(string filname);

    }
}
