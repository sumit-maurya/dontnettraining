﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassArray.Reposetory;

namespace ClassArray.Method
{
   
    internal class Product
    {
        public string Name { get; set; }
        public string Categories { get; set; }
        public int Price { get; set; }
        public int Rating { get; set; }


        public   Product(string name, string category ,int price ,int rating)
        {
            Name = name;
            Categories = category;
            Price = price;
            Rating = rating;

        }
    }
}
