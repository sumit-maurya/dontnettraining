﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shortedList
{
    internal class student
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id::{Id}\t Price::{Age}\tName::{Name}\n";
        }
    }
}

