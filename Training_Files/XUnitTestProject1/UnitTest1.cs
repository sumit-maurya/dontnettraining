using Tempcon;
namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Initialization
            var temp1 = 32;
            var expected = 89.6;
            //Act
            //Call the Development Code
            tempclass obj = new tempclass();
            double actual = obj.Cel2Fer(temp1);
            //Assert
            Assert.Equal(expected, actual);
            

        }
    }
}