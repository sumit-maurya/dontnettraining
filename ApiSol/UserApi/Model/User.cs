﻿namespace UserApi.Model.User
{
    public class User
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public string Location { get; set; }
        public bool IsBlocked { get; set; } = false;
    }
}
