﻿using Microsoft.EntityFrameworkCore;
using UserApi.Model.User;

namespace UserApi.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> Context):base(Context)
        {

        }
        public DbSet<User> UserList { get; set; }
    }
}
