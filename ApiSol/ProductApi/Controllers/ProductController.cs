﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Model;
using ProductApi.Reposetory;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductReposetory _productReposetory;
        public ProductController(IProductReposetory productReposetory)
        {
            _productReposetory = productReposetory;
        }
        [Route("GetAllProduct")]
        [HttpGet]
        public IActionResult GetALlProduct()
        {
            List<Product> products = _productReposetory.GetAllProduct();
            return Ok(products);
        }
        [Route("AddProduct")]
        [HttpPost]
        public IActionResult AddProduct(Product product)
        {
            bool addProductStatus = _productReposetory.AddProduct(product);
            return Created("api/created",addProductStatus);
        }
        [Route("GetProductById/{id:int}")]
        [HttpGet]
        public IActionResult GetProductById(int id)
        {
            Product product = _productReposetory.GetProductById(id);
            if (product == null)
            {
                return BadRequest($"Product {id} is not Found !!!");
            }
            else
            {
                return Ok(product);

            }
        }
    }
}
