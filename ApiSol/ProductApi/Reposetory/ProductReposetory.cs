﻿using ProductApi.Model;

namespace ProductApi.Reposetory
{
    public class ProductReposetory:IProductReposetory
    {
        List<Product> products;
        public ProductReposetory()
        {
            products = new List<Product>()
            {
                new Product() {Id=1,Name="Fan",Category="Elect" }
            };
        }

        public bool AddProduct(Product product)
        {
            products.Add(product);
            return true;
        }

        public List<Product> GetAllProduct()
        {
            return products;
        }

        public Product GetProductById(int id)
        {
            Product product = products.First(x => x.Id == id);
            return product;
        }
    }
}
