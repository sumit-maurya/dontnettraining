﻿using ProductApi.Model;

namespace ProductApi.Reposetory
{
    public interface IProductReposetory
    {
        public List<Product> GetAllProduct();
        bool AddProduct(Product product);
        Product GetProductById(int id);
    }
}
