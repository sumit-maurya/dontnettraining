﻿using Day15_Cookies.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Day15_Cookies.Controllers
{
    public class HomeController : Controller
    {
        //[HttpGet]
        //public IActionResult Index()
        //{
        //    return View();
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }
        [HttpPost]
        public IActionResult Index(User user)
        {
            CookieOptions option = new();
            option.Expires = DateTime.Now.AddMinutes(10);
            Response.Cookies.Append("Name", $"{user.Name}", option);
            return RedirectToAction("Dashboard");
        }

        public ActionResult Dashboard()
        {
            ViewBag.UserName = Request.Cookies["Name"];
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}