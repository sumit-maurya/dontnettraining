﻿namespace FoodApp_MVC.Execptions
{
    public class UserCredentialInvalidException:Exception
    {
        public UserCredentialInvalidException()
        {

        }
        public UserCredentialInvalidException(string message):base(message)
        {

        }
    }
}
