﻿using FoodApp_MVC.Context;
using FoodApp_MVC.Models;

namespace FoodApp_MVC.Repository
{
    public class UserRepository:IUserRepository
    {
        UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        #region Login Functions
        public User GetUserByUserName(string userName)
        {
            return _userDbContext.UserloginInfo.Where(u=>u.UserName==userName).FirstOrDefault();
        }
        public List<FoodItems> GetAllFoodItems()
        {
            return _userDbContext.FoodItems.ToList();
        }
       
        public bool RegisterUser(User user)
        {
            if (user.Role == "0")
                user.Role = "admin";
            else
                user.Role = "user";
            _userDbContext.UserloginInfo.Add(user);
            return _userDbContext.SaveChanges() == 1 ? true : false;
        }
        public User LogIn(string name, string password)
        {
            return _userDbContext.UserloginInfo.Where(u => u.UserName == name && u.Password == password).FirstOrDefault();
        }
        #endregion

        #region User functions
        public List<CartItems> GetFoodCart(int userId)
        {
            List<CartItems> allCartItems = _userDbContext.FoodCart
                                .Join(_userDbContext.UserloginInfo,
                                cart => cart.UserId,
                                user => user.UserId,
                                (cart, user) => new
                                {
                                    UserId = user.UserId,
                                    OrderId = cart.OrderId,
                                    FoodId=cart.FoodId,
                                    TotalPrice=cart.TotalPrice,
                                    Quantity=cart.Quantity
                                })
                                .Join(_userDbContext.FoodItems,
                                cart=>cart.FoodId,
                                items=>items.FoodId,
                                (cart, items) => new CartItems
                                {
                                    UserId = cart.UserId,
                                    OrderId = cart.OrderId,
                                    TotalPrice = cart.TotalPrice,
                                    Quantity = cart.Quantity,
                                    FoodName=items.FoodName,
                                    FoodCategory=items.FoodCategory,
                                    ImgPath=items.ImgPath
                                }
                                ).Where(x => x.UserId == userId).ToList();
            
            return allCartItems;
        }
        public bool AddtoCart(FoodCart foodItem)
        {
            if (_userDbContext.FoodCart.Any(x => x.FoodId == foodItem.FoodId && x.UserId==foodItem.UserId ))
            {
                var item = _userDbContext.FoodCart.First(x=> x.FoodId==foodItem.FoodId);
                item.TotalPrice += (item.TotalPrice / item.Quantity);
                item.Quantity += 1;
                return _userDbContext.SaveChanges() == 1 ? true : false;
            }
            _userDbContext.FoodCart.Add(foodItem);
            return _userDbContext.SaveChanges() == 1 ? true : false;
        }
       
        public int GetPriceByFoodId(int foodId)
        {
            int price= (from FoodItems in _userDbContext.FoodItems
                       where FoodItems.FoodId == foodId select FoodItems.Price).FirstOrDefault();
            return price;
        }

        public void UpdateQuantity(int orderId, int newQuantity)
        {
            
            var item = _userDbContext.FoodCart.First(x => x.OrderId == orderId);
            if (newQuantity <= 0)
            {
                _userDbContext.FoodCart.Remove(item);
                _userDbContext.SaveChanges();
                return;
            }
            item.TotalPrice = (item.TotalPrice / item.Quantity)*newQuantity;
            item.Quantity = newQuantity;
            _userDbContext.SaveChanges();
        }

        public void DeleteCartItem(int orderId)
        {
           var item= _userDbContext.FoodCart.First(x=>x.OrderId==orderId);
            _userDbContext.FoodCart.Remove(item);
            _userDbContext.SaveChanges();

        }

        public int RemoveCartItemsById(int id)
        {
            var allCart = _userDbContext.FoodCart.Where(x => x.UserId == id);
            int sum = allCart.Sum(x => x.TotalPrice);
            _userDbContext.FoodCart.RemoveRange(allCart);
            _userDbContext.SaveChanges();
            return sum;
        }
        #endregion

        #region Admin function

        public void UpdateFoodItem(FoodItems food)
        {
            var foodItem = _userDbContext.FoodItems.First(x => x.FoodId == food.FoodId);
            foodItem.FoodName = food.FoodName;
            if (food.FoodCategory == "1")
            {
                foodItem.FoodCategory = "NonVeg";
            }
            else if (food.FoodCategory == "0")
            {
                foodItem.FoodCategory = "Veg";
            }
           
            foodItem.ImgPath = food.ImgPath;
            foodItem.Price = food.Price;
            _userDbContext.SaveChanges();
        }

        public void DeleteFoodItem(int foodId)
        {
            FoodItems food = _userDbContext.FoodItems.First(x => x.FoodId == foodId);
            _userDbContext.FoodItems.Remove(food);
            _userDbContext.SaveChanges();
        }
       

        public bool AddFoodItems(FoodItems food)
        {
            if (!(_userDbContext.FoodItems.Any(x => x.FoodName == food.FoodName)))
            {
                _userDbContext.FoodItems.Add(food);
                _userDbContext.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion
    }
}
