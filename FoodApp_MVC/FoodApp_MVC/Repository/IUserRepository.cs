﻿using FoodApp_MVC.Models;

namespace FoodApp_MVC.Repository
{
    public interface IUserRepository
    {
        
        public bool RegisterUser(User user);
        public User GetUserByUserName(string userName);
        public User LogIn(string name, string password);
        public bool AddtoCart(FoodCart foodItem);

        public List<FoodItems> GetAllFoodItems();
        public List<CartItems> GetFoodCart(int userId);
        public int GetPriceByFoodId(int foodId);
        public void UpdateQuantity(int orderId, int newQuantity);
        public void DeleteCartItem(int id);
        public int RemoveCartItemsById(int id);
        public void UpdateFoodItem(FoodItems food);
        public void DeleteFoodItem(int FoodId);
        public bool AddFoodItems(FoodItems food);

    }
}
