﻿using FoodApp_MVC.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodApp_MVC.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext>Context):base(Context)
        {

        }
        public DbSet<User> UserloginInfo { get; set; }
        public DbSet<FoodItems> FoodItems { get; set; }
        public DbSet<FoodCart> FoodCart { get; set; }
        public DbSet<Cetagories> Cetagories { get; }
    }
}
