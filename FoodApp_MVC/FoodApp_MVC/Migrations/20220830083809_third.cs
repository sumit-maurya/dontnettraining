﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodApp_MVC.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                 name: "Role",
                 table: "UserloginInfo",
                 type: "nvarchar(max)",
                 nullable: false,
                 defaultValue: "");
        }
    }
}
