﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApp_MVC.Models
{
    public class FoodCart
    {
        [Key]
        public int OrderId { get; set; }
        [Required]
        [ForeignKey("FoodItems")]
        public int FoodId { get; set; }
        public virtual FoodItems FoodItems { get; set; }
        public int TotalPrice { get; set; }
        public int Quantity { get; set; }
        [Required]
        [ForeignKey("User")]
        public int? UserId { get; set; }
        public User User { get; set; }

    }
}
