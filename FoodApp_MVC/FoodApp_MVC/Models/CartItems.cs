﻿using Microsoft.EntityFrameworkCore;

namespace FoodApp_MVC.Models
{
    
    public class CartItems
    {
       
        public int UserId;
       
        public int OrderId;
        public int TotalPrice;
        public int Quantity;
        public string FoodName;
        public string FoodCategory;
        public string ImgPath;
                                  
       
    }
}
