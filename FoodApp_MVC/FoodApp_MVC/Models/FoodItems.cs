﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApp_MVC.Models
{
    public class FoodItems
    {
        [Key]
        public int FoodId { get; set; }
        public string FoodName { get; set; }

        
        [Required]
        [ForeignKey("Cetagories")]
        public string FoodCategory { get; set; }
        public virtual Cetagories Cetagories { get; set; }
        public  int Price { get; set; }
        public string ImgPath { get; set; }

    }
}
