﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace FoodApp_MVC.Models
{
    
    public class User
    {

        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        
        public string Password { get; set; }
        public string Role { get;set; }
    }
}
