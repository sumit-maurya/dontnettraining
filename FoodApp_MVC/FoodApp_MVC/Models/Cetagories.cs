﻿using System.ComponentModel.DataAnnotations;

namespace FoodApp_MVC.Models
{
    public class Cetagories
    {
        [Key]   
        public string CategoryName { get; set; }
    }
}
