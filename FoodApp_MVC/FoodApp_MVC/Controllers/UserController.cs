﻿using FoodApp_MVC.Execptions;
using FoodApp_MVC.Models;
using FoodApp_MVC.Repository;
using FoodApp_MVC.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp_MVC.Controllers
{
    public class UserController : Controller
    {
        readonly IUserServices _userServices;
        public UserController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        User user = new();
        [HttpGet]
        public IActionResult UserRegisteration()
        {
            return View(user);
        }
        [HttpPost]
        public IActionResult UserRegisteration(User user)
        {
            if (ModelState.IsValid)
            {
                if (_userServices.RegisterUser(user))
                {
                    return RedirectToAction("SuccessRegister");
                }
                else
                {
                    return RedirectToAction("FailedRegister");
                }
            }
            return View(user);
        }
        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogIn(User logInfo)
        {
            try
            {
                User user = _userServices.LogIn(logInfo);
                HttpContext.Session.SetInt32("UserId", user.UserId);
                if (user.Role == "user")
                {
                    return RedirectToAction("ShowMenu", "Food");
                }
                else
                {
                    return RedirectToAction("AdminShowMenu", "Admin");
                }
                
            }
            catch (UserCredentialInvalidException ucie)
            {
                
                return RedirectToAction("FailedLogin");
            }
        }
        [HttpGet]
        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("LogIn");
        }
        public IActionResult SuccessRegister()
        {
            return View();
        }
        public IActionResult FailedRegister()
        {
            return View();
        }
        public IActionResult FailedLogin()
        {
            return View();
        }
        public IActionResult Index()
        {
            return View();
        }
       

    }
}
