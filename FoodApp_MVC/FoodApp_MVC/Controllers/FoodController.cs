﻿using FoodApp_MVC.Models;
using FoodApp_MVC.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace FoodApp_MVC.Controllers
{
    public class FoodController : Controller
    {
       
        readonly IUserServices _userServices;
        
        public FoodController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        public IActionResult ShowMenu()
        {
            List<FoodItems> mainMenu = _userServices.GetAllFoodItems();
            return View(mainMenu);
        }

        [HttpGet]
        public IActionResult ShowCart()
        {
            List<CartItems> cartItems = _userServices.GetAllFoodCart((int)HttpContext.Session.GetInt32("UserId"));
            return View(cartItems);
        }
        [HttpGet]
        public IActionResult EditCartItems(int id)
        {
            CartItems item = _userServices.GetAllFoodCart((int)HttpContext.Session.GetInt32("UserId")).First(x => x.OrderId == id);
            return View(item);
        }
        [HttpPost]
        public IActionResult EditCartItems(IFormCollection form)
        {
            _userServices.UpdateQuantity(Convert.ToInt16( form["OrderId"].ToString()),Convert.ToInt16( form["Quantity"].ToString()));
            return RedirectToAction("ShowCart");
        }
        [HttpGet]
        public IActionResult AddtoCart(int id)
        {
            int user_id = (int)HttpContext.Session.GetInt32("UserId");
            _userServices.AddToCart(id,user_id);
            return RedirectToAction("ShowMenu");
        }
        [HttpGet]
        public IActionResult DeleteCart(int id)
        {
            _userServices.DeleteCartById(id);
            return RedirectToAction("ShowCart");
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult BuyNow()
        {
            List<CartItems> cartItems = _userServices.GetAllFoodCart((int)HttpContext.Session.GetInt32("UserId"));
            ViewBag.sum=_userServices.DeleteCart((int)HttpContext.Session.GetInt32("UserId"));
            ViewBag.InVoiceNo = _userServices.InVoiceNo((int)HttpContext.Session.GetInt32("UserId"));
            return View(cartItems);
        }
    }
    
}
