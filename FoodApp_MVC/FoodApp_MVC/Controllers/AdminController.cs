﻿using FoodApp_MVC.Models;
using FoodApp_MVC.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp_MVC.Controllers
{
    public class AdminController : Controller
    {
        readonly IUserServices _userServices;
        public AdminController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        [HttpGet]
        public IActionResult AdminShowMenu()
        {
            List<FoodItems> mainMenu = _userServices.GetAllFoodItems();
            return View(mainMenu);
        }
        [HttpGet]
        public IActionResult AdminEdit(int id)
        {
            FoodItems foodItem = _userServices.GetFoodItemById(id);
            return View(foodItem);
        }
        [HttpPost]
        public IActionResult AdminEdit(IFormCollection form)
        {
            FoodItems foodItem = new() {FoodId=int.Parse(form["FoodId"]), FoodName = form["FoodName"], FoodCategory = form["FoodCategory"], Price =int.Parse( form["Price"]), ImgPath = form["ImgPath"] };
            _userServices.UpdateFoodItem(foodItem);
            return RedirectToAction("AdminShowMenu");
        }
        public IActionResult DeleteFoodItem(int id)
        {
            _userServices.DeleteFoodItem(id);
            return RedirectToAction("AdminShowMenu");
        }
        [HttpGet]
        public IActionResult AddNewFood()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddNewFood(FoodItems food)
        {
            TempData["ItemExist"]=_userServices.AddFoodItem(food);
            return RedirectToAction("AdminShowMenu");

        }

        
    }
}
