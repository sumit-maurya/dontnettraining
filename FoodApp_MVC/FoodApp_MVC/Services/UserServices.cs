﻿using FoodApp_MVC.Execptions;
using FoodApp_MVC.Models;
using FoodApp_MVC.Repository;

namespace FoodApp_MVC.Services
{
    public class UserServices :IUserServices
    {
        readonly  IUserRepository _userRepository; 
        public UserServices(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<FoodItems> GetAllFoodItems()
        {
            return _userRepository.GetAllFoodItems();
        }
        public User LogIn(User logInfo)
        {
            User user = _userRepository.LogIn(logInfo.UserName, logInfo.Password);
            if (user != null)
            {
                
                return user;
            }
            else
            {
                throw new UserCredentialInvalidException($"{logInfo.UserName}and other details invalid");
            }
        }
        public bool RegisterUser(User user)
        {
            if (_userRepository.GetUserByUserName(user.UserName)==null){
                _userRepository.RegisterUser(user);
                return true;
            }
            return false;
        }
        public List<CartItems> GetAllFoodCart(int userId)
        {
            return _userRepository.GetFoodCart(userId);
        }

        public void AddToCart(int foodId,int userId)
        {   
            FoodCart foodItem = new FoodCart() { FoodId = foodId, TotalPrice = GetPricebyFoodId(foodId), Quantity = 1, UserId = userId };
            _userRepository.AddtoCart(foodItem);
        }

        public int GetPricebyFoodId(int foodId)
        {
            return _userRepository.GetPriceByFoodId(foodId) ;
        }

        public void UpdateQuantity(int orderId, int newQuantity)
        {
            _userRepository.UpdateQuantity(orderId,newQuantity);
        }
        public void DeleteCartById(int orderId)
        {
            _userRepository.DeleteCartItem(orderId);
        }

        public int DeleteCart(int userId)
        {
            return _userRepository.RemoveCartItemsById(userId);
        }

        public string InVoiceNo(int OrderId)
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            string second = DateTime.Now.Second.ToString();
            string inVoiceNo = "#" + year + month + day + "-" + hour + minute + second+OrderId.ToString();
            return inVoiceNo;
        }

        public FoodItems GetFoodItemById(int id)
        {
            FoodItems foodItem=_userRepository.GetAllFoodItems().First(x=>x.FoodId==id);
            return foodItem;
        }

        public void UpdateFoodItem(FoodItems foodItem)
        {
            _userRepository.UpdateFoodItem(foodItem);
        }

        public void DeleteFoodItem(int foodId)
        {
            _userRepository.DeleteFoodItem(foodId);
        }

        public bool AddFoodItem(FoodItems food)
        {
            if (food.FoodCategory == "1")
            {
                food.FoodCategory = "NonVeg";
            }
            else if (food.FoodCategory == "0")
            {
                food.FoodCategory = "Veg";
            }
            return _userRepository.AddFoodItems(food);
        }
    }
}
