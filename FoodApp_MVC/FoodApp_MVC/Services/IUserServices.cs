﻿using FoodApp_MVC.Models;

namespace FoodApp_MVC.Services
{
    public interface IUserServices
    {
       
        public bool RegisterUser(User user);
        public List<FoodItems> GetAllFoodItems();
        public User LogIn(User logInfo);
        public List<CartItems> GetAllFoodCart(int userId);
        public void AddToCart(int foodId,int userId);
        public int GetPricebyFoodId(int foodId);
        public void UpdateQuantity(int orderId, int newQuantity);
        public void DeleteCartById(int orderId);
        public int DeleteCart(int userId);
        public string InVoiceNo(int OrderId);
        public FoodItems GetFoodItemById(int id);
        public void UpdateFoodItem(FoodItems foodItem);
        public void DeleteFoodItem(int foodId);
        public bool AddFoodItem(FoodItems food);
    }
}
