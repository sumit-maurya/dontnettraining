﻿using System;
using System.Collections.Generic;

namespace FoodApp_withMVC.Models
{
    public partial class Category
    {
        public Category()
        {
            FoodItems = new HashSet<FoodItem>();
        }

        public string CategoryName { get; set; } = null!;

        public virtual ICollection<FoodItem> FoodItems { get; set; }
    }
}
