﻿using System;
using System.Collections.Generic;

namespace FoodApp_withMVC.Models
{
    public partial class FoodCart
    {
        public int OrderId { get; set; }
        public int FoodId { get; set; }
        public int TotalPrice { get; set; }
        public int Quantity { get; set; }
        public int UserId { get; set; }

        public virtual FoodItem Food { get; set; } = null!;
        public virtual UserloginInfo User { get; set; } = null!;
    }
}
