﻿using System;
using System.Collections.Generic;

namespace FoodApp_withMVC.Models
{
    public partial class UserloginInfo
    {
        public UserloginInfo()
        {
            FoodCarts = new HashSet<FoodCart>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; } = null!;
        public string Password { get; set; } = null!;

        public virtual ICollection<FoodCart> FoodCarts { get; set; }
    }
}
