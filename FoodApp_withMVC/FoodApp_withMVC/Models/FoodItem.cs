﻿using System;
using System.Collections.Generic;

namespace FoodApp_withMVC.Models
{
    public partial class FoodItem
    {
        public FoodItem()
        {
            FoodCarts = new HashSet<FoodCart>();
        }

        public int FoodId { get; set; }
        public string FoodName { get; set; } = null!;
        public string? FoodCategory { get; set; }
        public int Price { get; set; }
        public string? ImgPath { get; set; }

        public virtual Category? FoodCategoryNavigation { get; set; }
        public virtual ICollection<FoodCart> FoodCarts { get; set; }
    }
}
