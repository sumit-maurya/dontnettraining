﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FoodApp_withMVC.Models
{
    public partial class FoodAppDbContext : DbContext
    {
        public FoodAppDbContext()
        {
        }

        public FoodAppDbContext(DbContextOptions<FoodAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<FoodCart> FoodCarts { get; set; } = null!;
        public virtual DbSet<FoodItem> FoodItems { get; set; } = null!;
        public virtual DbSet<UserloginInfo> UserloginInfos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=DESKTOP-VUA8SUS\\MSSQLSERVER01;Database=FoodAppDb;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.CategoryName)
                    .HasName("PK__Categori__8517B2E1B5B21B7A");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FoodCart>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PK__FoodCart__C3905BCFF72CCB94");

                entity.ToTable("FoodCart");

                entity.HasOne(d => d.Food)
                    .WithMany(p => p.FoodCarts)
                    .HasForeignKey(d => d.FoodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__FoodCart__FoodId__440B1D61");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.FoodCarts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__FoodCart__UserId__44FF419A");
            });

            modelBuilder.Entity<FoodItem>(entity =>
            {
                entity.HasKey(e => e.FoodId)
                    .HasName("PK__FoodItem__856DB3EB207D1004");

                entity.Property(e => e.FoodCategory)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FoodName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.ImgPath)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.FoodCategoryNavigation)
                    .WithMany(p => p.FoodItems)
                    .HasForeignKey(d => d.FoodCategory)
                    .HasConstraintName("FK__FoodItems__FoodC__412EB0B6");
            });

            modelBuilder.Entity<UserloginInfo>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("UserloginInfo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
