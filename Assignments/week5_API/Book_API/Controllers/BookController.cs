﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using System.Text;

namespace Book_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        [HttpPost]
        [Route("BookOrder")]
        public ActionResult BookOrder()
        {
            string bookingMessaage = "Order Placed Successfully ";
            Byte[] bookingMessageInBytes = Encoding.UTF8.GetBytes(bookingMessaage);

            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.Uri = new Uri("amqp://sumit:sumit@Localhost:5672");
            var connection = connectionFactory.CreateConnection();
            var channel = connection.CreateModel();

            channel.ExchangeDeclare("Sumit-BookingExchange",ExchangeType.Fanout,true, false);
            channel.BasicPublish("Sumit-BookingExchange","",null,bookingMessageInBytes);
            channel.Close();
            connection.Close();

            return Ok("success");
        }
        [HttpPost]
        [Route("CencelOrder")]
        public ActionResult CencelOrder()
        {
            return Ok("Success !!!");
        }

    }
}
