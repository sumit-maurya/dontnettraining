﻿namespace User_API.Model
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Mobile { get; set; }
        public string Role { get; set; }
    }
}
