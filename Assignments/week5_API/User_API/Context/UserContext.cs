﻿using Microsoft.EntityFrameworkCore;
using User_API.Model;

namespace User_API.Context
{
    public class UserContext:DbContext
    {
        public UserContext(DbContextOptions<UserContext>Context):base(Context)
        {

        }
        public DbSet<User> UserTbl { get; set; }
    }
}
