﻿using User_API.Model;

namespace User_API.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUser();
        bool AddUser(User user);
        bool Delete(int id);
        User GetUserByName(string name);
        User GetUserById(int id);
        bool Edit(User user);
        User Login(string name, string password);
    }
}
