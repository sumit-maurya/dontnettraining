﻿using User_API.Context;
using User_API.Model;

namespace User_API.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserContext _userDbContext;
        public UserRepository(UserContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public bool AddUser(User user)
        {
            _userDbContext.UserTbl.Add(user);
            return _userDbContext.SaveChanges() == 1 ? true : false;
        }

        public bool Delete(int id)
        {
            if (_userDbContext.UserTbl.Any(x => x.UserId == id))
            {
                var user = _userDbContext.UserTbl.First(x => x.UserId == id);
                _userDbContext.UserTbl.Remove(user);
                return true;
            }
            return false;
        }

        public bool Edit(User user)
        {
            if (_userDbContext.UserTbl.Any(x => x.UserId == user.UserId))
            {
                var userInfo = _userDbContext.UserTbl.First(x => x.UserId == user.UserId);
                userInfo.Name = user.Name;
                userInfo.Password = user.Password;
                userInfo.Mobile = userInfo.Mobile;
                userInfo.Role = user.Role;
                return true;
            }
            return false;
        }

        public List<User> GetAllUser()
        {
            return _userDbContext.UserTbl.ToList();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.UserTbl.First(x => x.UserId == id);
        }

        public User GetUserByName(string name)
        {
            return _userDbContext.UserTbl.First(x => x.Name == name);
        }

        public User Login(string name, string password)
        {
            return _userDbContext.UserTbl.FirstOrDefault(x => x.Name == name && x.Password == password);
        }
    }
}
