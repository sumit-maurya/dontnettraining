﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

ConnectionFactory connectionFactory = new ConnectionFactory();
connectionFactory.Uri = new Uri("amqp://sumit:sumit@Localhost:5672");
var connection = connectionFactory.CreateConnection();
var channel = connection.CreateModel();

channel.QueueDeclare("Sumit-Fanout-Queue",true,false,false);
channel.QueueBind("Sumit - Fanout - Queue", "Sumit-BookingExchange","");

var eventingBasicConsumer = new EventingBasicConsumer(channel);

eventingBasicConsumer.Received += (Sender, EventArgs) =>
{
    var bookingResponseMessage = Encoding.UTF8.GetString(EventArgs.Body.ToArray());
    Console.WriteLine(bookingResponseMessage);
};

