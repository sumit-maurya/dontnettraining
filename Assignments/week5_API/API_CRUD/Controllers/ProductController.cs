﻿using API_CRUD.Model;
using API_CRUD.Reposetory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_CRUD.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductReposetory _productReposetory;
        public ProductController(IProductReposetory productReposetory)
        {
            _productReposetory = productReposetory;
        }

        [Route("GetAllProduct")]
        [HttpGet]
        public ActionResult GetAllProduct()
        {
           List<Product> product= _productReposetory.GetAllProduct();
            return Ok(product);
        }
        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            if (_productReposetory.AddProduct(product))
            {
                return Ok();
            }
            else
            {
                return BadRequest(" this Id's Product Already Present");
            }

        }
        [Route("DeleteProductById")]
        [HttpDelete]
        public ActionResult DeleteProductById(int id)
        {
            if (_productReposetory.DeleteProductById(id))
            {
                return Ok();
            }
            return BadRequest($"{id}is not Present in the Record");
        }
        [Route("UpdateProductById")]
        [HttpPut]
        public ActionResult UpdateProduct(Product product)
        {
            if (_productReposetory.UpdateProduct(product))
            {
                return Ok();
            }
            return BadRequest($"Id :{product.Id} is not present in record !!! ");
        }
    }
}
