﻿using API_CRUD.Model;

namespace API_CRUD.Reposetory
{
    public interface IProductReposetory
    {
        public bool AddProduct(Product product);
        public bool DeleteProductById(int id);
        public bool UpdateProduct(Product product);
        public List<Product> GetAllProduct();
    }
}
