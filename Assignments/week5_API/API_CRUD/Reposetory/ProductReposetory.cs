﻿
using API_CRUD.Model;

namespace API_CRUD.Reposetory
{
    public class ProductReposetory : IProductReposetory
    {
        List<Product> products;
        
        public ProductReposetory()
        {
            products = new List<Product>()
            {
                new Product(){Id=1,Name="Potato",Category= "Veg"},
                new Product(){Id=2,Name="pen",Category="school" },
                new Product(){Id=3,Name="color",Category="Art" }
            };
        }
       

        public bool AddProduct(Product product)
        {
            if(!products.Any(x=>x.Id==product.Id))
            {
                products.Add(product);
                return true;
            }
            return false;
            
        }

        public bool DeleteProductById(int id)
        {
            if (products.Any(x => x.Id == id))
            {
                var product = products.First(x => x.Id == id);
                products.Remove(product);
              
                return true;
            }
            return false;
        }

        public List<Product> GetAllProduct()
        {
            return products;
        }

        public bool UpdateProduct(Product product)
        {
            if (products.Any(x => x.Id == product.Id))
            {
                var productItem = products.First(x => x.Id ==product.Id );
                productItem.Name = product.Name;
                productItem.Category = product.Category;
                return true;
            }
            return false;
        }
        
    }
}
