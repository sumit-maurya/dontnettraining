﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_task3_Indexer
{
    internal class IndexerClass
    {
        int id;
        string userName;
        string password;

        public IndexerClass(int id, string name, string pass)
        {
            this.id = id;
            this.userName = name;
            this.password = pass;
        }
        //<acessModifier> <returnType> this[parameters]
        public object this[int index]
        {
            get
            {
                if (index == 1085)
                {
                    return id;
                }
                else if (index == 2087) return userName;
                else if (index == 3994) return password;
                return null;
            }
            set
            {
                if (index == 1085) id = (int)value;
                else if (index == 2087) userName = (string)value;
                else if (index == 3994) password = (string)value;
            }
        }
    }
}
