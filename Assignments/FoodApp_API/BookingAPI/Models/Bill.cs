﻿namespace BookingAPI.Models
{
    public class Bill
    {
        public string UserName { get; set; }
        public int Amount { get; set; }
        public string InvoiceNo { get; set; }

    }
}
