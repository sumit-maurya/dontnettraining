﻿using System.ComponentModel.DataAnnotations;

namespace BookingAPI.Models
{
    public class FoodCart
    {
        [Key]
        public int OrderId { get; set; }
        public string FoodName { get; set; }
        public string FoodCategory { get; set; }
        public string FoodDiscription { get; set; }
        public int Price { get; set; }
        public string? ImgPath { get; set; }
        public int Quantity { get; set; }
        public string UserName { get; set; }
    }
}
