﻿using BookingAPI.Models;
using BookingAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Metadata.Ecma335;

namespace BookingAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        readonly IBookingServices _bookingSrevices;
        public BookingController(IBookingServices bookingsrevices)
        {
            _bookingSrevices = bookingsrevices;
        }

        [Route("GetAllFoodCart")]
        [HttpGet]
        public IActionResult GetAllFoodCart(string userName)
        {
            List<FoodCart> foodCarts = _bookingSrevices.GetAllFoodCart(userName);
            return Ok(foodCarts);
        }

        [Route("AddToCart")]
        [HttpPost]
        public IActionResult AddToCart(FoodCart foodItem)
        {
            bool status= _bookingSrevices.AddToCart(foodItem);
            return Ok(status);
        }
        [Route("UpdateQuantity")]
        [HttpPut]
        public IActionResult UpdateQuantity(int[] parm)
        {
            bool status = _bookingSrevices.UpdateQuantity(parm[0], parm[1]);
           
            return Ok(status);
            
        }
        [Route("DeleteCartById")]
        [HttpDelete]
        public IActionResult DeleteCartById(int orderId)
        {
            bool status = _bookingSrevices.DeleteCartById(orderId);
            return Ok(status);
        }
        [Route("GetBill")]
        [HttpPost]
        public IActionResult GetBill(string[] user) //user[0]=userName ,user[1]=userEmail
        {
            Bill bill = new Bill()
            {
                UserName = user[0],
                Amount = _bookingSrevices.ReturnBill(user[0]),
                InvoiceNo = _bookingSrevices.InVoiceNo()
            };
            _bookingSrevices.SendEmail(user[1], bill);
            return Ok(bill);
        }
        [Route("GetCartById")]
        [HttpGet]
        public IActionResult GetCartById(int id)
        {
            FoodCart food = _bookingSrevices.GetFoodById(id);
            return Ok(food);
        }
       
    }
}
