﻿using BookingAPI.Models;

namespace BookingAPI.Services
{
    public interface IBookingServices
    {
        public List<FoodCart> GetAllFoodCart(string userName);
        public FoodCart GetFoodById(int OrderId);
        public bool AddToCart(FoodCart foodItem);
        public bool UpdateQuantity(int orderId, int newQuantity);
        public bool DeleteCartById(int orderId);
        public int ReturnBill(string userName);
        public string InVoiceNo();
        public void SendEmail(string userEmail, Bill userBill);


        }
}
