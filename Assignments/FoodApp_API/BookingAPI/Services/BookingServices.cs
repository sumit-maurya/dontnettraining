﻿using BookingAPI.Models;
using BookingAPI.Repository;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit.Text;
using MimeKit;
using MailKit.Net.Smtp;
using System.Reflection.Metadata;
using HandlebarsDotNet;

namespace BookingAPI.Services
{
    public class BookingServices:IBookingServices
    {
        readonly IBookingRepository _repository;
        private IConfiguration _configuration;

        public BookingServices(IBookingRepository repository,IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }
        public List<FoodCart> GetAllFoodCart(string userName)
        {
            return _repository.GetFoodCart(userName);
        }
        public bool AddToCart(FoodCart foodItem)
        {
            return _repository.AddtoCart(foodItem);
        }
       
        public bool UpdateQuantity(int orderId, int newQuantity )
        {
            return _repository.UpdateQuantity(orderId, newQuantity);
        }
        public bool DeleteCartById(int orderId)
        {
            return _repository.DeleteCartItem(orderId);
        }
        public int ReturnBill(string userName)
        {
            return _repository.RemoveCartItemsById(userName);
        }

        public string InVoiceNo()
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            string second = DateTime.Now.Second.ToString();
            string inVoiceNo = "#" + year + month + day + "-" + hour + minute + second ;
            return inVoiceNo;
        }

        public FoodCart GetFoodById(int OrderId)
        {
            return _repository.GetFoodByOrderId(OrderId);
        }
        public void SendEmail(string userEmail,Bill userBill)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(userEmail));
            email.Subject = "Invoice Details";
          
            var data = new
            {
                UserName = userBill.UserName,
                Amount = userBill.Amount,
                InvoiceNo = userBill.InvoiceNo,
                url = "https://cdn-icons-png.flaticon.com/512/8212/8212602.png"
            };
            var template = Handlebars.Compile(htmlTemplate());
            var result = template(data);
            email.Body = new TextPart(TextFormat.Html) { Text =result };
           // email.Attachments=
            var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email)
;
            smtp.Disconnect(true);
        }
        public string htmlTemplate()
        {
            string html = @" <div class=""entry"">         <div style=""display:flex ;flex-direction:row"">           <span><img src=""https://cdn-icons-png.flaticon.com/512/8212/8212602.png""></span>         <span>                <h1><strong>Order placed successfully!</strong></h1>               <h4>Invoice No.</h4><h4><strong style=""color:grey"">{{InvoiceNo}}</strong></h4>           </span>       </div>   <hr>       <h4>Dear <strong style=""color:red""> {{UserName}}</strong>,</h4>       <div>            <h4>Order Details</h4>         <h4 >Total Bill Amount::<strong style=""color:green"">Rs.{{Amount}}</strong></h4>       </div>   </div>";
            return html;
        }
    }
}
