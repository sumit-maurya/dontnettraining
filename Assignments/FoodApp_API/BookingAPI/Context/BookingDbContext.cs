﻿using BookingAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace BookingAPI.Context
{
    public class BookingDbContext:DbContext
    {
        public BookingDbContext(DbContextOptions<BookingDbContext>context):base(context)
        {

        }
        public DbSet<FoodCart> FoodCartTb { get; set; }
    }
}
