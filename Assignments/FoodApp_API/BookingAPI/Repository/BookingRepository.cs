﻿using BookingAPI.Context;
using BookingAPI.Models;

namespace BookingAPI.Repository
{
    public class BookingRepository:IBookingRepository
    {
        readonly BookingDbContext _bookingDbContext;
        public BookingRepository(BookingDbContext bookingDbContext)
        {
            _bookingDbContext = bookingDbContext;
        }
        public List<FoodCart> GetFoodCart(string userName)
        {
           
            return _bookingDbContext.FoodCartTb.Where(x=>x.UserName==userName).ToList();
        }
        public bool AddtoCart(FoodCart food)
        {
            if (_bookingDbContext.FoodCartTb.Any(x => x.FoodName == food.FoodName && x.UserName == food.UserName))
            {
                var item = _bookingDbContext.FoodCartTb.First(x => x.FoodName == food.FoodName && x.UserName==food.UserName);
                item.Price += (item.Price / item.Quantity);
                item.Quantity += 1;
                return _bookingDbContext.SaveChanges() == 1 ? true : false;
            }
            _bookingDbContext.FoodCartTb.Add(food);
            return _bookingDbContext.SaveChanges() == 1 ? true : false;
        }

        public FoodCart GetFoodByOrderId(int Id)
        {
            FoodCart food= _bookingDbContext.FoodCartTb.FirstOrDefault(x=>x.OrderId==Id );
            return food;
        }

        public bool UpdateQuantity(int orderId, int newQuantity )
        {

            var item = _bookingDbContext.FoodCartTb.FirstOrDefault(x => x.OrderId == orderId );
            if (item != null)
            {
                if (newQuantity <= 0)
                {
                    _bookingDbContext.FoodCartTb.Remove(item);
                    _bookingDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    item.Price = (item.Price / item.Quantity) * newQuantity;
                    item.Quantity = newQuantity;
                    _bookingDbContext.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool DeleteCartItem(int orderId)
        {
            FoodCart food = _bookingDbContext.FoodCartTb.FirstOrDefault(x => x.OrderId == orderId );
            if (food != null)
            {
                _bookingDbContext.FoodCartTb.Remove(food);
                _bookingDbContext.SaveChanges();
                return true;
            }
            return false;

        }

        public int RemoveCartItemsById(string userName)
        {
            var allCart = _bookingDbContext.FoodCartTb.Where(x => x.UserName==userName );
            if (allCart.Any())
            {
                int sum = allCart.Sum(x => x.Price);
                _bookingDbContext.FoodCartTb.RemoveRange(allCart);
                _bookingDbContext.SaveChanges();
                return sum;
            }
            else
            {
                return 0;
            }
            
        }
    }
}
