﻿using BookingAPI.Models;

namespace BookingAPI.Repository
{
    public interface IBookingRepository
    {
        public List<FoodCart> GetFoodCart(string userName);
        public bool AddtoCart(FoodCart food);
        public FoodCart GetFoodByOrderId(int Id);
        public bool UpdateQuantity(int orderId, int newQuantity);
        public bool DeleteCartItem(int orderId);
        public int RemoveCartItemsById( string userName);
    }
}
