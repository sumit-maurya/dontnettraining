using UserLoginAPI.Model;
using UserLoginAPI.Repository;
using UserLoginAPI.Services;

namespace UserLogin_API_unitTest
{
    public class UnitTest1:IClassFixture<UserLoginDbFixture>
    {
        readonly ILoginRepository _loginRepository;
        readonly ILoginServices _loginServices;
        readonly UserLoginDbFixture _userLoginDbFixture;
        public UnitTest1(UserLoginDbFixture userLoginDbFixture)
        {
            _userLoginDbFixture=userLoginDbFixture;
            _loginRepository = new LoginRepository(userLoginDbFixture._userLoginDbContext);
            _loginServices=new LoginServices(_loginRepository); 
        }

        [Fact]
        public async Task GetAllFoodItem_Test()
        {
            //Arrange
            var expectedTableCount = _userLoginDbFixture._userLoginDbContext.RegistrationTb.Count();
            //Act
            List<UserDetails> data = await _loginServices.GetAllUsers();
            var actualTableCount=data.Count();
            //Assert
            Assert.Equal(expectedTableCount, actualTableCount);
        }

        [Fact]
        public void Login_test()
        {
            //Arrange
            var expectedReturnObject= new UserDetails()
            {
                Name = "sumit",
                Password = "sumit123",
                Email = "sumit@gmail.com",
                Location = "mumbai",
                Role = "user"
            };
            var inputUserObject = new User()
            {
                UserName = expectedReturnObject.Name,
                Password = expectedReturnObject.Password
            };
            //Act
            UserDetails actualReturnObject=_loginServices.LogIn(inputUserObject);
            //assert
            Assert.NotNull(actualReturnObject);
            Assert.Equal(expectedReturnObject.Name, actualReturnObject.Name);
            Assert.Equal(expectedReturnObject.Password, actualReturnObject.Password);
            Assert.Equal(expectedReturnObject.Email, actualReturnObject.Email);
            Assert.Equal(expectedReturnObject.Location, actualReturnObject.Location);
            Assert.Equal(expectedReturnObject.Role, actualReturnObject.Role);
        }

        [Fact]
        public void Login_withInvaild_input_Test()
        {
            //Arrange
            var inputUserObject = new User()
            {
                UserName = " ",
                Password = " "
            };
            //Act
            UserDetails actualReturnObject = _loginServices.LogIn(inputUserObject);
            //assert
            Assert.Null(actualReturnObject);
        }

        [Fact]
        public void EditUser_with_ExistingUser()
        {
            //Arrange
            var expectedReturnStatus = true;
            var newValueForUpdate = new UserDetails()
            {
                Name = "ajay",
                Password = "ajay123",
                Email = "ajay@gmail.com",
                Location = "mumbai",
                Role = "user"
            };
            //Act
            var actualReturnStaus = _loginServices.EditUser(newValueForUpdate);
            //Assert
            Assert.Equal(expectedReturnStatus,actualReturnStaus);
        }

        [Fact]
        public void EditUser_with_NonExistingUser()
        {
            //Arrange
            var expectedReturnStatus = false;
            var newValueForUpdate = new UserDetails()
            {
                Name = "ajaykumar",
                Password = "ajay123",
                Email = "ajay@gmail.com",
                Location = "mumbai",
                Role = "user"
            };
            //Act
            var actualReturnStaus = _loginServices.EditUser(newValueForUpdate);
            //Assert
            Assert.Equal(expectedReturnStatus, actualReturnStaus);
        }

        [Fact]
        public void RegisterUser_With_ExistingUser()
        {
            //Arrange
            var expectedReturnStatus = false;
            var existEntry = new UserDetails()
            {
                Name = "sumit",
                Password = "sumit123",
                Email = "sumit@gmail.com",
                Location = "mumbai",
                Role = "user"
            };
            //Act
            var actualReturnStatus=_loginServices.RegisterUser(existEntry);
            //Assert
            Assert.Equal(expectedReturnStatus, actualReturnStatus);
        }

        [Fact]
        public void RegisterUser_with_NonExistingUser()
        {
            //Arrange
            var expectedReturnStatus = true;
            var existEntry = new UserDetails()
            {
                Name = "raj",
                Password = "sumit123",
                Email = "sumit@gmail.com",
                Location = "mumbai",
                Role = "user"
            };
            var expectedTableCount = _userLoginDbFixture._userLoginDbContext.RegistrationTb.Count()+1;
            //Act
            var actualReturnStatus = _loginServices.RegisterUser(existEntry);
            var actualTableCount = _userLoginDbFixture._userLoginDbContext.RegistrationTb.Count();
            //Assert
            Assert.Equal(expectedTableCount, actualTableCount);
            Assert.Equal(expectedReturnStatus, actualReturnStatus);
        }

        [Fact]
        public void Delete_NonExistingUser()
        {
            //Arrange
            bool expectedReturnStatus=false;
            //Act
            bool actualReturnStatus = _loginServices.DeleteUser(" ");
            //Assert
            Assert.Equal(expectedReturnStatus,actualReturnStatus);
        }

        [Fact]
        public void Delete_ExistingUser()
        {
            //Arrange
            bool expectedReturnStatus = true;
            var expectedTableLenghth = _userLoginDbFixture._userLoginDbContext.RegistrationTb.Count()-1;
            //Act
            bool actualReturnStatus = _loginServices.DeleteUser("amit");
            var actualTableLenghth = _userLoginDbFixture._userLoginDbContext.RegistrationTb.Count();
            //Assert
            Assert.Equal(expectedReturnStatus, actualReturnStatus);
            Assert.Equal(expectedTableLenghth, actualTableLenghth);
        }
    }
}