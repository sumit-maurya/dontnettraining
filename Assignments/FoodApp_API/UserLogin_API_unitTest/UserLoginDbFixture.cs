﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserLoginAPI.Context;
using UserLoginAPI.Model;

namespace UserLogin_API_unitTest
{
    public  class UserLoginDbFixture
    {
        internal UserLoginDbContext _userLoginDbContext;
        public UserLoginDbFixture()
        {
            var userLoginDbContextOptions=new DbContextOptionsBuilder<UserLoginDbContext>().UseInMemoryDatabase("UserLoginTextDb").Options;
            _userLoginDbContext=new UserLoginDbContext(userLoginDbContextOptions);
            _userLoginDbContext.Add(
                new UserDetails()
                {
                    Name="sumit",
                    Password="sumit123",
                    Email="sumit@gmail.com",
                    Location="mumbai",
                    Role="user"
                });
            _userLoginDbContext.Add(
                new UserDetails()
                {
                    Name = "ajay",
                    Password = "sumit123",
                    Email = "ajay@gmail.com",
                    Location = "mumbai",
                    Role = "user"
                });
            _userLoginDbContext.Add(
                new UserDetails()
                {
                    Name = "Vinit",
                    Password = "vinit123",
                    Email = "sumit@gmail.com",
                    Location = "mumbai",
                    Role = "admin"
                });
            _userLoginDbContext.Add(
                new UserDetails()
                {
                    Name = "amit",
                    Password = "amit123",
                    Email = "sumit@gmail.com",
                    Location = "mumbai",
                    Role = "admin"
                });
            _userLoginDbContext.SaveChanges();
        }
    }
}
