using BookingAPI.Context;
using BookingAPI.Models;
using BookingAPI.Repository;
using BookingAPI.Services;
using Microsoft.Extensions.Configuration;

namespace BookingAPi_Test
{
    public class UnitTest1:IClassFixture<BookingDbFixture>
    {
        readonly IBookingRepository _bookingRepository;
        readonly IBookingServices _bookingServices;
        readonly BookingDbContext _bookingDbContext;
        readonly IConfiguration _configuration;
        public UnitTest1(BookingDbFixture bookingDbFixture)
        {
            _bookingDbContext=bookingDbFixture._bookingDbContext;
            _bookingRepository = new BookingRepository(bookingDbFixture._bookingDbContext);
            _bookingServices = new BookingServices(_bookingRepository,_configuration);
        }

        [Fact]
        public void GetAllCartItem_Test()
        {
            //Arrange
            var expectedCartLength = _bookingDbContext.FoodCartTb.Count(x=>x.UserName=="amit");
            //Act
            var returnCartList = _bookingServices.GetAllFoodCart("amit");
            var actualCartLength=returnCartList.Count();
            //Assert
            Assert.Equal(actualCartLength, expectedCartLength);
        }

        [Fact]
        public void AddFoodItem_with_NonExisting_User()
        {
            //Arrange
            var expectedStatus = true;
            var expectedTableLength = _bookingDbContext.FoodCartTb.Count()+1;  //+1 for successful addition
            var foodItem = new FoodCart()
            {
                FoodName = "rice",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 80,
                ImgPath = "RiceImage",
                UserName = "ram",
                Quantity = 2
            };
            //Act
            var actualStatus = _bookingServices.AddToCart(foodItem);
            var actualTableLength=_bookingDbContext.FoodCartTb.Count();
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
            Assert.Equal(actualTableLength, expectedTableLength);
        }

        [Fact]
        public void AddFoodItem_with_Existing_User_withNonExisting_CartItem()
        {
            //Arrange
            var expectedStatus = true;
            var expectedTableLength = _bookingDbContext.FoodCartTb.Count() + 1;
            var foodItem = new FoodCart()
            {
                FoodName = "dal",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 80,
                ImgPath = "RiceImage",
                UserName = "sumit",
                Quantity = 2
            };
            //Act
            var actualStatus = _bookingServices.AddToCart(foodItem);
            var actualTableLength = _bookingDbContext.FoodCartTb.Count();
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
            Assert.Equal(actualTableLength, expectedTableLength);
        }

        [Fact]
        public void AddFoodItem_with_Existing_User_withSameFoodItem()
        {
            //Arrange
            var expectedStatus = true;
            var expectedTableLength = _bookingDbContext.FoodCartTb.Count() ;
            var foodItem = new FoodCart()
            {
                FoodName = "rice",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 80,
                ImgPath = "RiceImage",
                UserName = "sumit",
                Quantity = 1
            };
            //Act
            var actualStatus = _bookingServices.AddToCart(foodItem);
            var actualTableLength = _bookingDbContext.FoodCartTb.Count();
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
            Assert.Equal(actualTableLength, expectedTableLength);
        }

        [Fact]
        public void GetFoodItemById_test()
        {
            //Arrange
            var expectedFoodItem = new FoodCart()
            {
                OrderId = 4,
                FoodName = "fish",
                FoodCategory = "Nonveg",
                FoodDiscription = "side food",
                Price = 30,
                ImgPath = "SosImage",
                UserName = "som",
                Quantity = 3
            };
            //Act
            var actualFoodItem = _bookingServices.GetFoodById(4);
            //Assert
            Assert.Equal(expectedFoodItem.FoodName, actualFoodItem.FoodName);
            Assert.Equal(expectedFoodItem.UserName, actualFoodItem.UserName);
            Assert.Equal(expectedFoodItem.Quantity, actualFoodItem.Quantity);
            Assert.Equal(expectedFoodItem.Price, actualFoodItem.Price);
            Assert.Equal(expectedFoodItem.ImgPath, actualFoodItem.ImgPath);
        }

        [Fact]
        public void GetTotalBillAmount_test_withExistingUser()
        {
            //Arrange
            var expectedBillAmount = _bookingDbContext.FoodCartTb.Where(x => x.UserName == "amit").Sum(x => x.Price);
            //Act
            var actualBillAmount = _bookingServices.ReturnBill("amit");
            //Assert
            Assert.Equal(expectedBillAmount, actualBillAmount);
        }

        [Fact]
        public void GetTotalBillAmount_test_withNonExistingUser()
        {
            //Arrange
            var expectedBillAmount = 0;
            //Act
            var actualBillAmount = _bookingServices.ReturnBill(" ");
            //Assert
            Assert.Equal(expectedBillAmount, actualBillAmount);
        }

        [Fact]
        public void UpadteCartItem_withNonExistingOrderId()
        {
            //Arrange
            var expectedStatus=false;
            //Act
            var actualStatus = _bookingServices.UpdateQuantity(0, 3);
            //Assert
            Assert.Equal(expectedStatus,actualStatus);
        }

        [Fact]
        public void UpadteCartItem_withExistingOrderId()
        {
            //Arrange
            var expectedStatus = true;
            int expectedQuantity = 3;
            //Act
            var actualStatus = _bookingServices.UpdateQuantity(2, expectedQuantity);
            var actualQuantity = _bookingDbContext.FoodCartTb.FirstOrDefault(x=>x.OrderId==2).Quantity;
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
            Assert.Equal(expectedQuantity, actualQuantity);
        }

        [Fact]
        public void UpadteCartItem_withExistingOrderId_andqantity_zero()
        {
            //Arrange
            var expectedStatus = true;
            //Act
            var actualStatus = _bookingServices.UpdateQuantity(1, 0);
            var actualCartItem = _bookingDbContext.FoodCartTb.FirstOrDefault(x => x.OrderId == 1);
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
            Assert.Null(actualCartItem);
        }

        [Fact]
        public void DeleteCartItems_test_withNonExistingId()
        {
            //Arrange
            var expectedStatus = false;
            //Act
            var actualStatus = _bookingServices.DeleteCartById(-1);
            //Assert
            Assert.Equal(expectedStatus,actualStatus);
        }

        [Fact]
        public void DeleteCartItems_test_withExistingId()
        {
            //Arrange
            var expectedStatus = true;
            //Act
            var actualStatus = _bookingServices.DeleteCartById(5);
            //Assert
            Assert.Equal(expectedStatus, actualStatus);
        }

    }
}