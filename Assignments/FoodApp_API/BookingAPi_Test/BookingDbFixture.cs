﻿using BookingAPI.Context;
using BookingAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingAPi_Test
{
    public class BookingDbFixture
    {
        internal BookingDbContext _bookingDbContext;
        public BookingDbFixture()
        {
            var bookingDbContextOptions = new DbContextOptionsBuilder<BookingDbContext>().UseInMemoryDatabase("BookingAPITestDb").Options;
            _bookingDbContext = new BookingDbContext(bookingDbContextOptions);
            _bookingDbContext=new BookingDbContext(bookingDbContextOptions);

            _bookingDbContext.Add(
                new FoodCart()
                {
                    OrderId=1,
                    FoodName="rice",
                    FoodCategory="veg",
                    FoodDiscription="good food",
                    Price=40,
                    ImgPath="RiceImage",
                    UserName="amit",
                    Quantity=1
                });
            _bookingDbContext.Add(
                new FoodCart()
                {
                    OrderId = 2,
                    FoodName = "rice",
                    FoodCategory = "veg",
                    FoodDiscription = "good food",
                    Price = 80,
                    ImgPath = "RiceImage",
                    UserName = "sumit",
                    Quantity = 2
                });
            _bookingDbContext.Add(
                new FoodCart()
                {
                    OrderId = 3,
                    FoodName = "sos",
                    FoodCategory = "veg",
                    FoodDiscription = "side food",
                    Price = 30,
                    ImgPath = "SosImage",
                    UserName = "amit",
                    Quantity = 3
                });
            _bookingDbContext.Add(
               new FoodCart()
               {
                   OrderId = 4,
                   FoodName = "fish",
                   FoodCategory = "Nonveg",
                   FoodDiscription = "side food",
                   Price = 30,
                   ImgPath = "SosImage",
                   UserName = "som",
                   Quantity = 3
               });
            _bookingDbContext.Add(
               new FoodCart()
               {
                   OrderId = 5,
                   FoodName = "samosa",
                   FoodCategory = "veg",
                   FoodDiscription = "side food",
                   Price = 30,
                   ImgPath = "SosImage",
                   UserName = "som",
                   Quantity = 3
               });
            _bookingDbContext.SaveChanges();
        }
    }
}
