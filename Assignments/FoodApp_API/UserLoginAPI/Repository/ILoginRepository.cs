﻿using UserLoginAPI.Model;

namespace UserLoginAPI.Repository
{
    public interface ILoginRepository
    {
        Task<List<UserDetails>> GetAllUsers();
        UserDetails GetUserByName(string name);
        bool RegisterUser(UserDetails user);
        int DeleteUser(UserDetails userExists);
        bool EditUser(UserDetails user);
        UserDetails LogIn(string name, string password);
    }
}
