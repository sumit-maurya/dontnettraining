﻿using Microsoft.EntityFrameworkCore;
using UserLoginAPI.Context;
using UserLoginAPI.Model;

namespace UserLoginAPI.Repository
{
    public class LoginRepository:ILoginRepository
    {
        //Model:User        => Table:UserLoginTb
        //Model:UserDetails => Table:RegistrationTb

        readonly UserLoginDbContext _context;
        public LoginRepository(UserLoginDbContext context)
        {
            _context = context;
        }


        public bool EditUser(UserDetails user)
        {
            if (_context.RegistrationTb.Any(x => x.Name == user.Name)){
                var userDetails = _context.RegistrationTb.First(x => x.Name == user.Name);
                userDetails.Email = user.Email;
                userDetails.Role = user.Role;
                userDetails.Password = user.Password;
                userDetails.Location = user.Location;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public async Task<List<UserDetails>> GetAllUsers()
        {
            return await _context.RegistrationTb.ToListAsync();

        }
        public int DeleteUser(UserDetails user)
        {
            _context.RegistrationTb.Remove(user);
            return _context.SaveChanges();
        }
       

        public UserDetails GetUserByName(string userName)
        {
            return _context.RegistrationTb.FirstOrDefault(x => x.Name == userName);
        }

        public UserDetails LogIn(string name, string password)
        {
            return _context.RegistrationTb.FirstOrDefault(x=>x.Name==name&& x.Password==password);   
        }

        public bool RegisterUser(UserDetails user)
        {
            if (!_context.RegistrationTb.Any(x => x.Name == user.Name))
            {
                _context.RegistrationTb.Add(user);
                _context.SaveChanges();
                return true;
            }
           
            return false;
        }
    }
}
