using Microsoft.EntityFrameworkCore;
using UserLoginAPI.Context;
using UserLoginAPI.Repository;
using UserLoginAPI.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var serverConnection = builder.Configuration.GetConnectionString("LocalServerConnection");
builder.Services.AddDbContext<UserLoginDbContext>(option => option.UseSqlServer(serverConnection));
builder.Services.AddControllers();
builder.Services.AddScoped<ILoginRepository, LoginRepository>();
builder.Services.AddScoped<ILoginServices,LoginServices>();
builder.Services.AddScoped<ITokenGenerator, TokenGenerator>();
builder.Services.AddCors();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();


app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());
app.UseAuthorization();

app.MapControllers();

app.Run();
