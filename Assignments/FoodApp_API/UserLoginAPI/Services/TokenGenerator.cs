﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace UserLoginAPI.Services
{
    public class TokenGenerator : ITokenGenerator
    {
        public string GenerateToken( string name,string role,string email)
        {
            var userClaims = new Claim[]
             {
                 new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                 new Claim(JwtRegisteredClaimNames.UniqueName,name)

             };

            var userSecurityKey = Encoding.UTF8.GetBytes("ssskljkjsiNHDisnjaaanjkdhc");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSiginCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                   issuer: "MVCCore",
                   audience: "CoreMVCCore",
                   claims: userClaims,
                   expires: DateTime.UtcNow.AddMinutes(10),
                   signingCredentials: userSiginCredentials
                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            string userJwtSecutrityTokenHandler = JsonConvert.SerializeObject(new { Token = userSecurityTokenHandler, Name = name,Role=role ,Email=email});
            return userJwtSecutrityTokenHandler;
        }
    }
}
