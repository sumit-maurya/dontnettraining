﻿using UserLoginAPI.Model;

namespace UserLoginAPI.Services
{
    public interface ILoginServices
    {
        Task<List<UserDetails>> GetAllUsers();
        bool RegisterUser(UserDetails user);
        bool DeleteUser(string userName);
        bool EditUser( UserDetails user);
      
        UserDetails LogIn(User loginUser);
    }
}
