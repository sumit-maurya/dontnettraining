﻿namespace UserLoginAPI.Services
{
    public interface ITokenGenerator
    {
        string GenerateToken(string name,string role,string email);
    }
}
