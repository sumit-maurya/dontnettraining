﻿using UserLoginAPI.Model;
using UserLoginAPI.Repository;

namespace UserLoginAPI.Services
{
    public class LoginServices:ILoginServices
    {
        readonly ILoginRepository _repository;
        public LoginServices(ILoginRepository repository)
        {
            _repository = repository;
        }

       
        public bool DeleteUser(string userName)
        {
            UserDetails userExists = _repository.GetUserByName(userName);
            if (userExists != null)
            {
                int userDeleteStatus = _repository.DeleteUser(userExists);
                if (userDeleteStatus==1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }    
        }

        public bool EditUser( UserDetails user)
        {
           return _repository.EditUser(user);
        }

        public async Task<List<UserDetails>> GetAllUsers()
        {
            return await _repository.GetAllUsers();
        }

        public UserDetails LogIn(User loginUser)
        {
            UserDetails user = _repository.LogIn(loginUser.UserName, loginUser.Password);
            return user;
        }

        public bool RegisterUser(UserDetails user)
        {
            return _repository.RegisterUser(user);
        }
    }
}
