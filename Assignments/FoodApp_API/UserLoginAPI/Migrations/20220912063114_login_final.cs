﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserLoginAPI.Migrations
{
    public partial class login_final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "UserLoginTb");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "UserLoginTb",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
