﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace UserLoginAPI.Model
{
    public class UserDetails
    {
        [Key]
       
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public string Role { get; set; }
    }
}
