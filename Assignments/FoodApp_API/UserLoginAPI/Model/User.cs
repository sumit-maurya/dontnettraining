﻿using System.ComponentModel.DataAnnotations;

namespace UserLoginAPI.Model
{
    public class User
    {
        [Key]
        public string UserName { get; set; }
        public string Password { get; set; }
        
    }
}
