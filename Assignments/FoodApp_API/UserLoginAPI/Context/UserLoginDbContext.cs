﻿using Microsoft.EntityFrameworkCore;
using UserLoginAPI.Model;

namespace UserLoginAPI.Context
{
    public class UserLoginDbContext:DbContext
    {
        public UserLoginDbContext(DbContextOptions<UserLoginDbContext>context):base(context)
        {

        }
        
        public DbSet<UserDetails> RegistrationTb { get; set; }
    }
}
