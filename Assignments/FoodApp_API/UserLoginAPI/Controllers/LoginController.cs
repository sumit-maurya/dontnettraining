﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserLoginAPI.Model;
using UserLoginAPI.Services;

namespace UserLoginAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        readonly ILoginServices _loginService;
        readonly ITokenGenerator _tokenGenerator;
        public LoginController(ILoginServices loginService, ITokenGenerator tokenGenerator)
        {
            _loginService = loginService;
            _tokenGenerator = tokenGenerator;
        }
        [Route("EditUser")]
        [HttpPut]
        public ActionResult EditUser(UserDetails user)
        {
            bool userEditStatus = _loginService.EditUser(user);
            return Ok(userEditStatus);

        }
        [Route("GetAllUsers")]
        [HttpGet]
        public async Task<ActionResult> GetAllUsers()
        {
            List<UserDetails> users = await _loginService.GetAllUsers();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(UserDetails user)
        {
            bool registerUserStatus = _loginService.RegisterUser(user);
            return Ok(registerUserStatus);
        }
        [Route("DeleteUser")]
        [HttpDelete]
        public ActionResult DeleteUser(string userName)
        {
            bool userDeleteStatus = _loginService.DeleteUser(userName);
            return Ok(userDeleteStatus);
        }
        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(User loginUser)
        {
            UserDetails user = _loginService.LogIn(loginUser);

            string userToken = _tokenGenerator.GenerateToken(user.Name, user.Role,user.Email);
            if (user!=null)
            {
                return Ok(userToken);
            }
            return BadRequest(userToken);
            
        }
    }
}
