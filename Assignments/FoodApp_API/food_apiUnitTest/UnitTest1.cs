using Food_API.Model;
using Food_API.Repository;
using Food_API.Services;
using Microsoft.AspNetCore.Authorization;

namespace food_apiUnitTest
{
    public class UnitTest1:IClassFixture<FoodDbFixture>
    {
        readonly IFoodRepository _foodRepository;
        readonly IFoodServices _foodServices;
        readonly FoodDbFixture _fixture;
        public UnitTest1(FoodDbFixture foodDbFixture)
        {
            _fixture = foodDbFixture;
            _foodRepository = new FoodRepository(foodDbFixture._foodItemsDbContext);
            _foodServices=new FoodServices(_foodRepository);
        }

        [Fact]
        public void GetAllFoodItems_Test()
        {
            //Arrange
            int excepted =_fixture._foodItemsDbContext.FoodItems.Count();
            //Act
            var items = _foodServices.GetAllFoodItems();
            //Assert
            Assert.Equal(excepted, items.Count);
        }

        [Fact]
        public void Update_ExistingItem_FoodItems_Test()
        {
            //Arrange
            var expected = true;
            var newfood = new FoodItems()
            {
                FoodId = 1,
                FoodName = "sos",
                FoodCategory = "Nonveg",
                FoodDiscription = "Good Food",
                Price = 30,
                ImgPath = "sosImage"
            };
            //Act
            bool actual = _foodServices.UpdateFoodItem(newfood);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Update_NonExistingItem_Test()
        {
            //Arrange
            var expected = false;
            var newfood = new FoodItems()
            {
                FoodId = 7,
                FoodName = "mango",
                FoodCategory = "Nonveg",
                FoodDiscription = "Good Food",
                Price = 30,
                ImgPath = "sosImage"
            };
            //Act
            bool actual = _foodServices.UpdateFoodItem(newfood);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddExisting_FoodItem_Test()
        {
            //arrange
            var expected = false;
            var newfood = new FoodItems()
            {
                
                FoodName = "sos",
                FoodCategory = "Nonveg",
                FoodDiscription = "Good Food",
                Price = 30,
                ImgPath = "sosImage"
            };
            //Act
            bool actual = _foodServices.AddFoodItem(newfood);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddNonExisting_foodItem_test()
        {
            //Arrange
            var expected = true;
            var newfood = new FoodItems()
            {
           
                FoodName = "samosa",
                FoodCategory = "Nonveg",
                FoodDiscription = "Good Food",
                Price = 30,
                ImgPath = "sosImage"
            };
            //Act
            bool actual = _foodServices.AddFoodItem(newfood);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetFoodItemById_Test_with_Existing_Item()
        {
            //Arrange
            var newfood = new FoodItems()
            {
                FoodId = 3,
                FoodName = "roti",
                FoodCategory = "veg",
                FoodDiscription = "Good Food",
                Price = 30,
                ImgPath = "sosImage"
            };
            //Act
            FoodItems actual = _foodServices.GetFoodItemById(3);
            //Assert
            Assert.NotNull(actual);
            Assert.Equal(newfood.FoodName, actual.FoodName);
            Assert.Equal(newfood.FoodCategory, actual.FoodCategory);
            Assert.Equal(newfood.Price, actual.Price);
            Assert.Equal(newfood.ImgPath, actual.ImgPath);
            Assert.Equal(newfood.FoodDiscription, actual.FoodDiscription);
        }

        [Fact]
        public void GetFoodItemById_Test_with_NonExisting_Item()
        {
           //Arrange

           //Act
            FoodItems actual = _foodServices.GetFoodItemById(0);
            //Assert
            Assert.Null(actual);
        }

        [Fact]
        public void DeleteFoodItem_NonExistingItem()
        {
            //Arrange
           bool isDeletedExpected=false;
            int ExpectedCountAfterDeletion = _fixture._foodItemsDbContext.FoodItems.Count() ;
            //Act
            bool isDeletedActual = _foodServices.DeleteFoodItem(5);
            int actualCountAfterDeletion = _fixture._foodItemsDbContext.FoodItems.Count();
            //Assert
            Assert.Equal(isDeletedExpected, isDeletedActual);
            Assert.Equal(ExpectedCountAfterDeletion, actualCountAfterDeletion);
        }

        [Fact]
        public void DeleteFoodItem_ExistingItem()
        {
            //Arrange
            bool isDeletedExpected = true;
            int ExpectedCountAfterDeletion = _fixture._foodItemsDbContext.FoodItems.Count()-1;
            //Act
            bool isDeletedActual = _foodServices.DeleteFoodItem(2);
            int actualCountAfterDeletion = _fixture._foodItemsDbContext.FoodItems.Count();
            //Assert

            Assert.Equal(isDeletedExpected, isDeletedActual);
            Assert.Equal(ExpectedCountAfterDeletion, actualCountAfterDeletion);
        }
    }
}