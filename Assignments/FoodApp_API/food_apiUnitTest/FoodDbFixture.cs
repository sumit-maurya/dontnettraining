﻿using Food_API.Context;
using Food_API.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace food_apiUnitTest
{
    public class FoodDbFixture
    {
        internal FoodItemsDbContext _foodItemsDbContext;
        public FoodDbFixture()
        {
            var foodDbContextOptions= new DbContextOptionsBuilder<FoodItemsDbContext>().UseInMemoryDatabase("testFoodDb").Options;
            _foodItemsDbContext = new FoodItemsDbContext(foodDbContextOptions);
            _foodItemsDbContext.Add(
                new FoodItems()
                {
                    FoodId = 1,
                    FoodName = "sos",
                    FoodCategory = "veg",
                    FoodDiscription = "Good Food",
                    Price = 30,
                    ImgPath = "sosImage"
                });
            _foodItemsDbContext.Add(
               new FoodItems()
               {
                   FoodId = 2,
                   FoodName = "cake",
                   FoodCategory = "veg",
                   FoodDiscription = "Good Food",
                   Price = 30,
                   ImgPath = "sosImage"
               });
            _foodItemsDbContext.Add(
                new FoodItems()
                {
                    FoodId=3,
                    FoodName="roti",
                    FoodCategory="veg",
                    FoodDiscription="Good Food",
                    Price=30,
                    ImgPath="sosImage"
                }
            );
            _foodItemsDbContext.SaveChanges();

        }
    }
}
