using Food_API.Context;
using Food_API.Controllers;
using Food_API.Model;
using Food_API.Repository;
using Food_API.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Food_api_xunit
{
    public class UnitTest1 
    {

        readonly Mock<FoodRepository> _repository;
      
        
        public UnitTest1()
        {
            _repository = new Mock<FoodRepository>();
         
        }

        [Fact]
        public void allfooditems_test()
        {
            //arrange
            FoodItems fooditemexpected = null;
            _repository.Setup(service => service.AddFoodItems(It.IsAny<FoodItems>())).Callback<FoodItems>(x => fooditemexpected = x);
            var fooditem = new FoodItems()
            {
                FoodName = "sos",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 20,
                ImgPath = "image"
            };

            //act
            _repository.Object.AddFoodItems(fooditem);

            //assert
            _repository.Verify(x => x.AddFoodItems(It.IsAny<FoodItems>()), Times.Once());
           Assert.Equal(fooditemexpected.FoodName, fooditem.FoodName);
            Assert.Equal(fooditemexpected.FoodCategory, fooditem.FoodCategory);
            Assert.Equal(fooditemexpected.FoodDiscription, fooditem.FoodDiscription);
            Assert.Equal(fooditemexpected.Price, fooditem.Price);
            Assert.Equal(fooditemexpected.ImgPath, fooditem.ImgPath);
            }
            [Fact]
        public void AddExisting_FoodItemTest()
        {
           
            
            var foodItem = new FoodItems()
            {
                FoodName = "Sos",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 20,
                ImgPath = "image"
            };
            _repository.Setup(x => x.AddFoodItems(It.IsAny<FoodItems>())).Returns(addToDatabase(foodItem));
            var _foodServices = new FoodServices(_repository.Object);
            //Act
            bool status= _foodServices.AddFoodItem(foodItem);
            //Assert
            //_foodServices.Verify(x => x.AddFoodItem(It.IsAny<FoodItems>()), Times.Once());
            Assert.False(status);

        }
        [Fact]
        public void AddNonExisting_FoodItemTest()
        {
          
            var newFood = new FoodItems()
            {
                FoodId=2,
                FoodName = "sos",
                FoodCategory = "veg",
                FoodDiscription = "good food",
                Price = 20,
                ImgPath = "image"
            };
            bool functionReturns = addToDatabase(newFood);
            _repository.Setup(x => x.AddFoodItems(It.IsAny<FoodItems>())).Returns(true);
            //Act
            bool status =_repository.Object.AddFoodItems(newFood);
            //Assert
            _repository.Verify(x => x.AddFoodItems(It.IsAny<FoodItems>()), Times.Once());
            Assert.True(status);

        }
        private List<FoodItems> GetTableData()
        {
            List<FoodItems> foodItems = new List<FoodItems>
            {
                new FoodItems()
                {
                    FoodId=1,
                    FoodName="sos",
                    FoodCategory="veg",
                    FoodDiscription="Good Food",
                    Price=30,
                    ImgPath="sosImage"
                },
                new FoodItems()
                {
                    FoodId=2,
                    FoodName="cake",
                    FoodCategory="veg",
                    FoodDiscription="Good Food",
                    Price=30,
                    ImgPath="sosImage"
                },
                new FoodItems()
                {
                    FoodId=3,
                    FoodName="roti",
                    FoodCategory="veg",
                    FoodDiscription="Good Food",
                    Price=30,
                    ImgPath="sosImage"
                }
            };
            return foodItems;
        }
        public bool addToDatabase(FoodItems food)
        {
            List<FoodItems> database = GetTableData();

        if (!(database.Any(x => x.FoodName == food.FoodName)))
            {
                database.Add(food);
                return true;
            }
            return false;
        }
    }
    
}