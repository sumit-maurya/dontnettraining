﻿using Food_API.Context;
using Food_API.Model;

namespace Food_API.Repository
{
    public class FoodRepository:IFoodRepository
    {
        readonly FoodItemsDbContext _foodDbContext;
        public FoodRepository(FoodItemsDbContext foodDbContext)
        {
            _foodDbContext = foodDbContext;
        }

        public List<FoodItems> GetAllFoodItems()
        {
            return _foodDbContext.FoodItems.ToList();
        }
        public bool UpdateFoodItem(FoodItems food)
        {
            var foodItem = _foodDbContext.FoodItems.FirstOrDefault(x => x.FoodId == food.FoodId);
            if (foodItem != null)
            {
                foodItem.FoodName = food.FoodName;
              
                foodItem.FoodCategory =food.FoodCategory;
                foodItem.FoodDiscription = food.FoodDiscription;

                foodItem.ImgPath = food.ImgPath;
                foodItem.Price = food.Price;
                _foodDbContext.SaveChanges();
                return true;
            }
            return false;
            
        }

        public bool DeleteFoodItem(int foodId)
        {
            FoodItems food = _foodDbContext.FoodItems.FirstOrDefault(x => x.FoodId == foodId);
            if (food != null)
            {
                _foodDbContext.FoodItems.Remove(food);
                _foodDbContext.SaveChanges();
                return true;
            }
            return false;
            
        }


        public bool AddFoodItems(FoodItems food)
        {
            if (!(_foodDbContext.FoodItems.Any(x => x.FoodName == food.FoodName)))
            {
                _foodDbContext.FoodItems.Add(food);
                _foodDbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public FoodItems GetFoodItemById(int FoodId)
        {
            return _foodDbContext.FoodItems.FirstOrDefault(x=>x.FoodId==FoodId);
        }
    }
}
