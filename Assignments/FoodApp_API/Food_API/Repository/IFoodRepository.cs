﻿using Food_API.Model;

namespace Food_API.Repository
{
    public interface IFoodRepository
    {
        public bool UpdateFoodItem(FoodItems food);
        public bool DeleteFoodItem(int FoodId);
        public bool AddFoodItems(FoodItems food);
        public List<FoodItems> GetAllFoodItems();
        public FoodItems GetFoodItemById(int FoodId);

    }
}
