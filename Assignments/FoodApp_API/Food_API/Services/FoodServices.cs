﻿using Food_API.Model;
using Food_API.Repository;

namespace Food_API.Services
{
    public class FoodServices:IFoodServices
    {
        readonly IFoodRepository _foodRepository;
        public FoodServices(IFoodRepository foodRepository)
        {
            _foodRepository = foodRepository;
        }
        public List<FoodItems> GetAllFoodItems()
        {
            return _foodRepository.GetAllFoodItems();
        }
        public bool UpdateFoodItem(FoodItems foodItem)
        {
           return _foodRepository.UpdateFoodItem(foodItem);
        }

        public bool DeleteFoodItem(int foodId)
        {
           return _foodRepository.DeleteFoodItem(foodId);
        }

        public bool AddFoodItem(FoodItems food)
        {
            if (food.FoodCategory == "1")
            {
                food.FoodCategory = "NonVeg";
            }
            else if (food.FoodCategory == "0")
            {
                food.FoodCategory = "Veg";
            }
            return _foodRepository.AddFoodItems(food);
        }

        public FoodItems GetFoodItemById(int id)
        {
            return _foodRepository.GetFoodItemById(id);
        }
    }
}
