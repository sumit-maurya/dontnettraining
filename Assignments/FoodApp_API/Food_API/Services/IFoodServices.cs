﻿using Food_API.Model;

namespace Food_API.Services
{
    public interface IFoodServices
    {
        public List<FoodItems> GetAllFoodItems();
        public bool UpdateFoodItem(FoodItems foodItem);
        public bool DeleteFoodItem(int foodId);
        public bool AddFoodItem(FoodItems food);
        FoodItems GetFoodItemById(int id);
    }
}
