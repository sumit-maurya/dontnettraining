﻿using System.ComponentModel.DataAnnotations;

namespace Food_API.Model
{
    public class FoodItems
    {
        [Key]
        public int FoodId { get; set; }
        public string FoodName { get; set; }
        public string FoodCategory { get; set; }
        public string FoodDiscription { get; set; }
        public int Price { get; set; }
        public string? ImgPath { get; set; }
    }
}
