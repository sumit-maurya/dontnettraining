﻿using Food_API.Model;
using Food_API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Food_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : ControllerBase
    {
        readonly IFoodServices _foodServices;
        public FoodController(IFoodServices foodServices)
        {
            _foodServices = foodServices;
        }

        [Route("GetAllFoodItems")]
        [HttpGet]
        public IActionResult GetAllFoodItems()
        {
           List<FoodItems> foodItems =_foodServices.GetAllFoodItems();
            if(foodItems != null)
            {
                return Ok(foodItems);
            }
            return BadRequest("Food Menu is Empty");
        }
        [Route("GetFoodItemById")]
        [HttpGet]
        public IActionResult GetFoodItemById(int id)
        {
            FoodItems foodItem = _foodServices.GetFoodItemById(id);
            if (foodItem != null)
            {
                return Ok(foodItem);
            }
            return BadRequest("Food is not Present");
        }

        [Route("FoodItemsEdit")]
        [HttpPut]
        public IActionResult FoodItemsEdit(FoodItems[] food)
        {
            bool status = _foodServices.UpdateFoodItem(food[0]);
            return Ok(status);
        }

        [Route("DeleteFoodItem/{id:int}")]
        [HttpDelete]
        public IActionResult DeleteFoodItem(int id)
        {
          bool status=  _foodServices.DeleteFoodItem(id);
            return Ok(status);
        }

        [Route("AddNewFood")]
        [HttpPost]
        public IActionResult AddNewFood(FoodItems food)
        {
            var AddFoodStatus = _foodServices.AddFoodItem(food);
            //if (AddFoodStatus==true)
            //{
            //    return Ok("Food Added successfully");
            //}
            //return BadRequest("Food Already Present ");  
            return Ok(AddFoodStatus);
        }
    }
}
