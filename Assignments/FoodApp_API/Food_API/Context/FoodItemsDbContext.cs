﻿using Food_API.Model;
using Microsoft.EntityFrameworkCore;

namespace Food_API.Context
{
    public class FoodItemsDbContext : DbContext
    {
        public FoodItemsDbContext(DbContextOptions<FoodItemsDbContext> context) : base(context)
        {

        }
        public DbSet<FoodItems> FoodItems {get;set;}
    }
}
