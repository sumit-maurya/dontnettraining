﻿using Microsoft.AspNetCore.Identity;

namespace Acount_Api_Identity.Model
{
    public class ApplicationUser:IdentityUser
    {
        public string? City { get; set; }
    }
}
