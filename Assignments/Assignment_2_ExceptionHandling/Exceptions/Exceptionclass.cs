﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Exceptions
{
    internal class Exceptionclass : Exception
    {
        public Exceptionclass()
        {

        }
        public Exceptionclass(string message) : base(message)
        {

        }

    }
}
