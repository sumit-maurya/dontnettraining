﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Reposetory
{
    internal class UserReposetory : IUserReposetory, IFile
    {
        List<User> users;

        public UserReposetory()
        {
            users = new List<User>();
        }

      

        public void ReadFromFile(string filname)
        {
            using (StreamReader sr = new StreamReader(filname))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine($"{line}");
                }

            }
        }

      
        public void RegisterUser(User user)
        {
            users.Add(user);
            string filename = "Data2.txt";
            ExceptionMethod objException = new ExceptionMethod();
            try
            {
                objException.IsNameThere(user.Name, filename);
                WriteToFile(user, filename);
                Console.WriteLine("Registorationn Successful");
            }
            catch (FileNotFoundException ex)
            {
                WriteToFile(user, filename);
            }
            catch (Exceptionclass ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void WriteToFile(User user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                sw.Write(user.ToString());
            }
        }
    }
}
