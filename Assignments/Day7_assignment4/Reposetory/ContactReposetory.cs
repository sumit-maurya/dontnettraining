﻿using Assignment_3_list.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3_list.Reposetory
{
    internal class ContactReposetory
    {
        List<Contact> contacts;

        public ContactReposetory()
        {
            contacts = new List<Contact>()
            {
                new Contact(){Name="Sumit",Address="dahisar",City="Mumbai",PhoneNumber="75957589359"},
                new Contact(){Name="Amit",Address="dahisar",City="Mumbai",PhoneNumber="7595755959"},
                new Contact(){Name="Vinit",Address="dahisar",City="Mumbai",PhoneNumber="7595755959"}

            };
        }
        

        public void AddContact(Contact objContact)
        {   
            
            if (contacts.FirstOrDefault(x=>x.Name==objContact.Name)==null)
            {
                contacts.Add(objContact);
                Console.WriteLine("Contact is Added successfully");
            }
            else
            {
                Console.WriteLine("Contact is Already Present");
            }
        }

        //private bool isNamePresent(string name)
        //{
        //    foreach(Contact contact in contacts)
        //    {
        //        if(contact.Name == name)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public string Delete (string name)
        {
            if (contacts.FirstOrDefault(x => x.Name == name) != null)
            {
                List<Contact> updatedList= contacts.Where(x=>x.Name!=name).ToList();

                contacts = updatedList;

               
                return "Contact is Deleted Successfully";
            }
            else
            {
                return "Contact Name is Not Present!!!";
            }
            
        }
        //public string Delete(string name)
        //{
        //    for (int i = 0; i < contacts.Count; i++)
        //    {
        //        if (contacts[i].Name == name)
        //        {
        //            contacts.RemoveAt(i);
        //            return "Contact is Deleted Successfully";
        //        }
        //    }
        //    return "Contact Name is Not Present!!!";
        //}

        public string Update(Contact ObjContact)
        {
            int index=contacts.FindIndex(x => x.Name == ObjContact.Name);
            if (index != -1)
            {
                contacts[index] = ObjContact;
                return " Contact is Updated Successfully";
            }
            else
            {
                return "Contact is not Found!!!";
            }
        }

        public List<Contact> GetAllContact()
        {
            return contacts;
        }


    }
}
