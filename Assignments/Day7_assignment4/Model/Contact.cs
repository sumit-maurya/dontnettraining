﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3_list.Model
{
    internal class Contact
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return $"Name::{Name}\tAddress::{Address}\tCity::{City}\tPhoneNumber::{PhoneNumber}\n";
        }
    }
}
