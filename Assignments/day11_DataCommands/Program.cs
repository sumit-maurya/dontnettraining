﻿// See https://aka.ms/new-console-template for more information
using day11_DataCommands.Model;
using day11_DataCommands.Reposetory;

Student student = new Student();

StudentReposetory studentReposetory = new StudentReposetory();

//Task-1--Display Details
foreach (Student studentInfo in studentReposetory.GetAllInfo())
{
    Console.WriteLine(studentInfo);
}


Console.WriteLine("---------------Add to database-----------------");
Console.WriteLine("Enter the Name");
student.Name = Console.ReadLine();
Console.WriteLine("Enter the RollNo::");
student.RollNo = int.Parse(Console.ReadLine());
studentReposetory.Add(student);
foreach (Student studentInfo in studentReposetory.GetAllInfo())
{
    Console.WriteLine(studentInfo);
}


Console.WriteLine("--------------Delete details------------------");
Console.WriteLine("Enter the Id for deletion::");
int id = int.Parse(Console.ReadLine());
studentReposetory.DeleteInfo(id);
foreach (Student studentInfo in studentReposetory.GetAllInfo())
{
    Console.WriteLine(studentInfo);
}


Console.WriteLine("---------------Update details of database-----------------");
Console.WriteLine("Enter the Id::");
student.Id=int.Parse(Console.ReadLine());
Console.WriteLine("Enter the New Name");
student.Name = Console.ReadLine();
Console.WriteLine("Enter the New RollNo::");
student.RollNo = int.Parse(Console.ReadLine());
studentReposetory.UpdateInfo(student);
foreach (Student studentInfo in studentReposetory.GetAllInfo())
{
    Console.WriteLine(studentInfo);
}

studentReposetory.DisplayWithDataTable();

studentReposetory.DisplayWithDataset();

