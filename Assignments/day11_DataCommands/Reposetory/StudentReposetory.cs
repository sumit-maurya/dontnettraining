﻿using day11_DataCommands.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day11_DataCommands.Reposetory
{
    internal class StudentReposetory
    {
        SqlConnection sqlconnect = null;
        SqlCommand cmd = null;

       //CRUD operations with dataCommands
        public StudentReposetory()
        {
            sqlconnect = new SqlConnection("Server=DESKTOP-VUA8SUS\\MSSQLSERVER01; Database=StudentDb1;Trusted_Connection=True");
            

        }
        public void Add(Student student)
        {
            try
            {
                string query = "Insert into studentDetails values (@name,@Roll)";
                cmd = new SqlCommand(query, sqlconnect);
                cmd.Parameters.AddWithValue("@name", student.Name);
                cmd.Parameters.AddWithValue("@Roll", student.RollNo);
                sqlconnect.Open();
                int addStatus = cmd.ExecuteNonQuery();
                if (addStatus > 0)
                {
                    Console.WriteLine("Data Insertion is Completed");
                }
                else
                {
                    Console.WriteLine("Insertion is failed!!");
                }
                sqlconnect.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Student> GetAllInfo()
        {
            try
            {
                List<Student> students = new List<Student>();
                cmd = new SqlCommand();
                cmd.CommandText = "Select * from studentDetails";
                cmd.Connection = sqlconnect;
                sqlconnect.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Student student = new Student();
                    student.Id = (int)reader["Id"];
                    student.Name = reader["studName"].ToString();
                    student.RollNo = (int)reader["rollNo"];
                    students.Add(student);
                }
                sqlconnect.Close();
                return students;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public void DeleteInfo(int id)
        {
            try
            {
                string query = "Delete from studentDetails where Id=@id";
                cmd = new SqlCommand(query, sqlconnect);
                cmd.Parameters.AddWithValue("@id", id);

                sqlconnect.Open();
                int addStatus = cmd.ExecuteNonQuery();
                if (addStatus > 0)
                {
                    Console.WriteLine("\nData Deletion is Completed\n");
                }
                else
                {
                    Console.WriteLine("\nDeletion is failed!!!\n");
                }
                sqlconnect.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public void UpdateInfo(Student student)
        {
            try
            {
                string query = "Update studentDetails set studName=@name,rollNo=@rollno where Id=@id";
                cmd = new SqlCommand(query, sqlconnect);
                cmd.Parameters.AddWithValue("@id", student.Id);
                cmd.Parameters.AddWithValue("@name", student.Name);
                cmd.Parameters.AddWithValue("@rollno", student.RollNo);

                sqlconnect.Open();
                int addStatus = cmd.ExecuteNonQuery();
                if (addStatus > 0)
                {
                    Console.WriteLine("\nData Updated Successfully\n");
                }
                else
                {
                    Console.WriteLine("\nData Updation is failed!!!\n");
                }
                sqlconnect.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Dispaly All the Studnets using Dataset and DataAdapter 
        public void DisplayWithDataset()
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("Select * from studentDetails",sqlconnect);

            DataSet dataset =new DataSet();
            sqlDataAdapter.Fill(dataset);
            Console.WriteLine("------------------Display All Element by Dataset------------");
            foreach(DataRow row  in dataset.Tables[0].Rows)
            {
                Console.WriteLine($"Id::{row["Id"]}\tName::{row["studName"]}\t{row["rollNo"]}");
            }
        }
        //Dispaly All the Users using DataAdapter and dataTable 
        public void DisplayWithDataTable()
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("Select * from studentDetails", sqlconnect);

            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            Console.WriteLine("------------------Display All Element by DataTable------------");
            foreach (DataRow row in dataTable.Rows)
            {
                Console.WriteLine($"Id::{row["Id"]}\tName::{row["studName"]}\t{row["rollNo"]}");
            }
        }
    }
}
