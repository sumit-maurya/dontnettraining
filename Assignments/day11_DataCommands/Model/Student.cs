﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day11_DataCommands.Model
{
    internal class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RollNo { get; set; }

        public override string ToString()
        {
            return $"Id::{Id}\tName::{Name}\tRollNo::{RollNo}";

        }
    }
}
