﻿// See https://aka.ms/new-console-template for more informatin
using ClassArray.Method;
using ClassArray.Reposetory;


ProductRep ProductR = new ProductRep();

Product[] allProduct = ProductR.GetAllProduct();
Console.WriteLine("-------------------------------\n");
foreach (Product item in allProduct)
{
    Console.WriteLine($"{item.Name} {item.Categories} {item.Price}  {item.Rating}");
};
Console.WriteLine("-------------------------------\n");



Console.WriteLine("You want to update the classArray ? Yes:Y or No:N");
string ans = Console.ReadLine();
if (ans == "Y")
{
    Console.WriteLine("----------Update------------");
    Console.WriteLine("Enter the index::");
    int ind = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter the Name::");
    string name1 = Console.ReadLine();

    Console.WriteLine("Enter the Category::");
    string categ = Console.ReadLine();

    Console.WriteLine("enter the price::");
    int price1 = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter the Rating::");
    int rating1 = int.Parse(Console.ReadLine());

    ProductR.UpdateClassArray(ind, name1, categ, price1, rating1);

    Product[] allProduct1 = ProductR.GetAllProduct();
    Console.WriteLine("-------------------------------\n");
    foreach (Product item in allProduct1)
    {

        Console.WriteLine($"{item.Name} {item.Categories} {item.Price}  {item.Rating}");

    };
    Console.WriteLine("-------------------------------\n");

}


Console.WriteLine("Delete the Element for the Class Array ? Yes:Y or No:N ");
ans = Console.ReadLine();
if (ans == "Y")
{
    Console.WriteLine("------------Delete------------");
    Console.WriteLine("Enter the index For Delete::");
    int indexD = int.Parse(Console.ReadLine());

    ProductR.Delete(indexD);
    Product[] allProduct1 = ProductR.GetAllProduct();

    Console.WriteLine("-------------------------------\n");
    foreach (Product item in allProduct1)
    {

        Console.WriteLine($"{item.Name} {item.Categories} {item.Price}  {item.Rating}");

    };
    Console.WriteLine("-------------------------------\n");
}


Console.WriteLine("You want to Add object to the classArray ? Yes:Y or No:N");
string ans1 = Console.ReadLine();

if (ans1 == "Y")
{

    Console.WriteLine("----------Add----------");
    Console.WriteLine("Enter the Name::");
    string name1 = Console.ReadLine();

    Console.WriteLine("Enter the Category::");
    string categ = Console.ReadLine();

    Console.WriteLine("enter the price::");
    int price1 = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter the Rating::");
    int rating1 = int.Parse(Console.ReadLine());

    Product objProduct = new Product(name1, categ, price1, rating1);

    ProductR.Add(objProduct);
    Product[] allProduct2 = ProductR.GetAllProduct();

    Console.WriteLine("-------------------------------\n");
    foreach (Product item in allProduct2)
    {
        Console.WriteLine($"{item.Name} {item.Categories} {item.Price}  {item.Rating}");
    };
    Console.WriteLine("-------------------------------\n");
}
Console.WriteLine("Get the Element for the Class Array ? Yes:Y or No:N ");
ans = Console.ReadLine();
if (ans == "Y")
{
    Console.WriteLine("------------Get Element------------");
    Console.WriteLine("Enter the Name For Details::");
    string in_name = Console.ReadLine();

    Product? product_details=ProductR.Finditem(in_name);

    if(product_details != null)
    {
        Console.WriteLine($"{product_details.Name} {product_details.Categories} {product_details.Price}  {product_details.Rating}");
    }
    else
    {
        Console.WriteLine("!!! Sorry Item is NOT Present !!!");
    }
    Console.WriteLine("-------------------------------\n");
}


