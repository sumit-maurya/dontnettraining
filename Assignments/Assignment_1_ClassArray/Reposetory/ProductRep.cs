﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassArray.Method;


namespace ClassArray.Reposetory
{

    internal class ProductRep
    {
        Product[] products;
        public ProductRep()
        {
            products = new Product[]
            {
                new Product("oppo","Mobile",5000,5),
                new Product("Vivo","Mobile",10000,3),
                new Product("Mi","TV",25000,5),
                new Product("MI","Mobile ",15000,5),
            };
        }
        public Product[] GetAllProduct()
        {
            return products;
        }
        public void UpdateClassArray(int index, string name1, string categ, int price1, int rating)
        {
            products[index].Name = name1;
            products[index].Categories = categ;
            products[index].Price = price1;
            products[index].Rating = rating;

        }
        public void Delete(int indexD)
        {
            int i = -1;
            foreach (Product p in products)
            {
                i++;
            }
            if ((indexD <= i) && (i != -1))
            {
                Product[] fproduct = new Product[i];
                int a = 0;
                foreach (Product item1 in products)
                {
                    if (item1 != products[indexD])
                    {
                        fproduct[a] = item1;
                        a++;
                    }
                }
                products = fproduct;
            }
            else if (i == -1)
            {
                Console.WriteLine("ClassArray is EMPTY");
            }
            else
            {
                Console.WriteLine($"index should be between 0 to {i} ");
            }
        }
        public void Add(Product objProduct)
        {
            int i = 0;
            foreach (Product p in products)
            {
                i++;
            }
            Product[] fproduct = new Product[i + 1];
            int a = 0;
            foreach (Product item1 in products)
            {


                fproduct[a] = item1;
                a++;

            }
            fproduct[a] = objProduct;
            products = fproduct;

        }
        public Product? Finditem(string name)
        {
            foreach(Product item in products)
            {
                if (item.Name == name)
                {
                    return item;
                }
            }  
            return null;
        }
    }
}
