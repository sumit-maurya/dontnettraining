﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_Task2.Model
{
    internal class Product1
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public override string ToString()
        {
            return $"ProductId::{ProductId}\tProductName::{ProductName}";
        }
    }
}
