﻿// See https://aka.ms/new-console-template for more information
using Day7_Task2.Model;

List<Product1> objProduct = new List<Product1>()
{
    new Product1() { ProductId = 1, ProductName = "Rice" },
    new Product1(){ProductId=2,ProductName="Bread"},
    new Product1(){ProductId= 3,ProductName="Milk"}
};

List<ProductDetails> objProductDetails = new List<ProductDetails>() {
    new ProductDetails(){ShoppingID=123,ProductID=3,Quantity=11},
    new ProductDetails(){ShoppingID=154,ProductID=6,Quantity=5},
    new ProductDetails(){ShoppingID=775,ProductID=4,Quantity=2},
    new ProductDetails(){ShoppingID=132,ProductID=2,Quantity=10},
    new ProductDetails(){ShoppingID=111,ProductID=1,Quantity=9}
};

var result = from p in objProduct
             join q in objProductDetails 
             on p.ProductId equals q.ProductID 
             select new
             {
               ProductId=p.ProductId,
               ProductName=p.ProductName,
               Quantity =q.Quantity
            };
foreach(var item in result)
{
    Console.WriteLine($"ProductId::{item.ProductId}\tProductName::{item.ProductName}\tQuantity::{item.Quantity}");
}