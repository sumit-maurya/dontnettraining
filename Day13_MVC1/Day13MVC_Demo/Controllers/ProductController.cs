﻿using Microsoft.AspNetCore.Mvc;

namespace Day13MVC_Demo.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(IFormCollection form)
        {
            int userId = int.Parse(form["Id"]);
            string name =form["Name"];

            return Content($"Id={userId}\tName={name}");

        }

    }
}
